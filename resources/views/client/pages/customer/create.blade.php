@extends('client.structure')
@section('client.title', 'Create New Customer')
@section('client.menu.customer', 'm-menu__item--active')
@section('client')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Dashboard
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Customer
											</span>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Create New Customer
											</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Create New Customer
                            </h3>
                        </div>
                    </div>


                </div>

                <!--begin::Form to add product-->
                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"  method="POST"
                      action="{{route('customer.store')}}">
                    {{ csrf_field() }}

                    <div class="m-portlet__body">

                        @if ($errors->any())

                        <div class="form-group m-form__group row">
                            <div class="col-lg-12">



                                    <div class="form-group m-form__group row">

                                        <div class="col-lg-12 col-md-12 col-sm-12">

                                            <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger alert-dismissible fade show" role="alert">
                                                <div class="m-alert__icon">
                                                    <i class="flaticon-exclamation-1"></i>
                                                    <span></span>
                                                </div>
                                                <div class="m-alert__text">
                                                    <strong>
                                                        OOPS! <br>
                                                    </strong>
                                                    @foreach ($errors->all() as $error)
                                                        {{ $error }} <br>

                                                    @endforeach
                                                </div>
                                                <div class="m-alert__close">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                            </div>
                        </div>

                        @endif

                        <div class="form-group m-form__group row">
                            <div class="col-lg-3">
                                <label>
                                    Customer Name:
                                </label>
                                <div class="input-group m-input-group m-input-group--square">
														<span class="input-group-addon">
															<i class="la la-institution"></i>
														</span>
                                <input type="text" name="name" class="form-control m-input--air" placeholder="Name">

                                </div>
                            </div>
                            <div class="col-lg-3">
                                <label class="">
                                    Customer Email:
                                </label>
                                <div class="input-group m-input-group m-input-group--square">
														<span class="input-group-addon">
															<i class="la la-envelope"></i>
														</span>

                                <input type="email" name="email" class="form-control m-input--air" placeholder="Email">

                                </div>
                            </div>
                            <div class="col-lg-3">
                                <label>
Mobile Number:
                                </label>
                                <div class="input-group m-input-group m-input-group--square">
														<span class="input-group-addon">
															<i class="la la-mobile"></i>
														</span>
                                    <input name="mobile" type="number" class="form-control m-input--air" placeholder="">
                                </div>
                            </div>


                            <div class="col-lg-3">
                                <label>
GSTIN:
                                </label>
                                <div class="input-group m-input-group m-input-group--square">
														<span class="input-group-addon">
															<i class="la la-info"></i>
														</span>
                                    <input name="gstin" type="text" class="form-control m-input--air" placeholder="GSTIN">
                                </div>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                                <div class="col-lg-4">
                                    <label>
                                        Contact Person Name:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
														<span class="input-group-addon">
															<i class="la la-user"></i>
														</span>
                                        <input type="text" name="person_name" class="form-control m-input--air" placeholder="Contact Person">

                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label class="">
                                        Street:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
														<span class="input-group-addon">
															<i class="la la-street-view"></i>
														</span>

                                        <input type="text" name="street" class="form-control m-input--air" placeholder="Street">

                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <label>
                                        Locality:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
														<span class="input-group-addon">
															<i class="la la-location-arrow"></i>
														</span>
                                        <input name="locality" type="text" class="form-control m-input--air" placeholder="Locality">
                                    </div>
                                </div>
                            </div>
                        <div class="form-group m-form__group row">
                                <div class="col-lg-3">
                                    <label>
                                        City:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
														<span class="input-group-addon">
															<i class="la la-map-marker"></i>
														</span>
                                        <input type="text" name="city" class="form-control m-input--air" placeholder="City">

                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <label class="">
                                        State:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
														<span class="input-group-addon">
															<i class="la la-map-pin"></i>
														</span>

                                        <input type="text" name="state" class="form-control m-input--air" placeholder="State">

                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <label>
                                        Country:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
														<span class="input-group-addon">
															<i class="la la-map-o"></i>
														</span>
                                        <input name="country" type="text" class="form-control m-input--air" placeholder="Country">
                                    </div>
                                </div>


                            <div class="col-lg-3">
                                    <label>
                                        Pin Code:
                                    </label>
                                    <div class="input-group m-input-group m-input-group--square">
														<span class="input-group-addon">
															<i class="la la-crosshairs"></i>
														</span>
                                        <input name="pin" type="number" class="form-control m-input--air" placeholder="Pin Code">
                                    </div>
                                </div>



                            </div>



                    </div>




                    <div class="m-form m-form--fit m-form--label-align-right">
                        <div class="form-group m-form__group row">
                            <div class="row">
                                <div class="col-lg-9">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        Submit
                                    </button>
                                </div>
                                <div class="col-lg-3">

                                    <button type="reset" class="btn btn-secondary">
                                        Reset
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>



                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
    @endsection

@section('client_footer')
    <script src="{{ asset('js/seclect2.js')}}" type="text/javascript"></script>
@endsection