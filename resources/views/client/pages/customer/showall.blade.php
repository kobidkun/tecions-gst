@extends('client.structure')
@section('client.title', 'All Customers')
@section('client.menu.customer', 'm-menu__item--active')
@section('client')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.min.css">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Dashboard
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Customer
											</span>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												All Customer
											</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->


        <div class="m-content">
            <div class="row">
                <div class="col-xl-12">
                    <!--begin::Portlet-->
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">All Customer

                                    </h3>

                                </div>
                            </div>

                            <div class="m-portlet__head-tools">
                                <ul class="m-portlet__nav">
                                    <li class="m-portlet__nav-item">
                                        <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">
                                            <a href="{{route('product.create')}}" class="btn btn-outline-primary m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air">
															<span>
																<i class="la la-industry"></i>
																<span>
																	Add New Product
																</span>
															</span>
                                            </a>

                                        </div>
                                    </li>
                                </ul>
                            </div>



                        </div>












                        <div class="m-portlet__body">
                            <!--begin::Section-->
                            <div class="m-section">



                                <div class="m-section__content">
                                    <table class="table table-bordered" id="users-table">
                                        <thead>
                                        <tr>
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Mobile</th>
                                            <th>GstIn</th>
                                            <th>City</th>
                                        </tr>
                                        </thead>
                                    </table>



                                </div>








                            </div>




    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection

@section('client_footer')
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script>
        $(function() {
            $('#users-table').DataTable({
                serverSide: true,
                oLanguage: {sProcessing: '<div class="" style="width: 500px; display: inline-block;z-index: 700"><img src="{{asset('/loader/ajaxloader.gif')}}" alt=""></div>'},

                ajax: '{!! route('api.get.customer') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { data: 'name', name: 'name' },
                    { data: 'mobile', name: 'mobile' },
                    { data: 'gstin', name: 'gstin' },
                    { data: 'city', name: 'city' }
                ]
            });
        });
    </script>
@endsection

{{--

<table class="table m-table m-table--head-bg-success">
    <thead>
    <tr>
        <th>
            Product Name
        </th>
        <th>
            HSN Code
        </th>
        <th>
            IGST
        </th>
        <th>
            SGST
        </th>
        <th>
            CGST
        </th>

        <th>
            Price
        </th>

        <th>
            Created At
        </th>

        <th>
            Action
        </th>
    </tr>
    </thead>
    <tbody>

    @foreach($product as $p)

        <tr>
            <th scope="row">
                {{$p->name}}
            </th>
            <td>
                {{$p->hsn}}
            </td>
            <td>
                {{$p->igst}}%
            </td>
            <td>
                {{$p->sgst}}%
            </td>

            <td>
                {{$p->cgst}}%
            </td>

            <td>
                <i class="la la-rupee"></i> {{$p->opeaning_rate}}
            </td>

            <td>
                {{ date("d M Y", strtotime($p->created_at))}}
            </td>

            <td>
                <button type="button" class="btn m-btn--pill m-btn--air         btn-outline-primary m-btn m-btn--outline-2x ">
                    View Details
                </button>
                <a href="{{route('product.edit', ['id' => $p->id])}}">
                    <button type="button" class="btn m-btn--pill m-btn--air
                                                       btn-outline-info m-btn m-btn--outline-2x ">
                        Edit
                    </button>
                </a>
                <form action="{{route('product.destroy', ['id' => $p->id])}}" style="display: inline" method="post">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button type="submit" class="btn m-btn--pill m-btn--air
                                                          btn-outline-danger m-btn m-btn--outline-2x ">
                        Delete
                    </button>
                </form>


            </td>
        </tr>
    @endforeach
    </tbody>
</table>

--}}
