@extends('client.structure')
@section('client.title', 'Details of invoice')

@section('client')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Dashboard
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Sale Invoice
											</span>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Create New Sale Incoice
											</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Create Sales Invoice
                            </h3>
                        </div>
                    </div>


                </div>

                <!--begin::Form to add product-->
                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                    {{ csrf_field() }}

                    <div class="m-portlet__body">

                        @if ($errors->any())

                            <div class="form-group m-form__group row">
                                <div class="col-lg-12">


                                    <div class="form-group m-form__group row">

                                        <div class="col-lg-12 col-md-12 col-sm-12">

                                            <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger alert-dismissible fade show"
                                                 role="alert">
                                                <div class="m-alert__icon">
                                                    <i class="flaticon-exclamation-1"></i>
                                                    <span></span>
                                                </div>
                                                <div class="m-alert__text">
                                                    <strong>
                                                        OOPS! <br>
                                                    </strong>
                                                    @foreach ($errors->all() as $error)
                                                        {{ $error }} <br>

                                                    @endforeach
                                                </div>
                                                <div class="m-alert__close">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                            aria-label="Close"></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>

                        @endif

                        <div class="form-group m-form__group row">
                            <div class="col-lg-4">
                                <label>
                                    Invoice Id:
                                </label>
                                <input type="text" name="inv_id"
                                       autocomplete="off"
                                       class="form-control m-input--air"
                                       placeholder="Required">
                                <span class="m-form__help">
														Invoice Id
													</span>
                            </div>
                            <div class="col-lg-4">
                                <label class="">
                                    Date of Invoice:
                                </label>
                                <input type='text' name="inv_date_of_purchase" class="form-control"
                                       value="{{ date('d/m/Y') }}" id="m_datepicker_1"
                                       readonly placeholder="Select date"/>

                                <span class="m-form__help">
														Date of invoice
													</span>
                            </div>
                            <div class="col-lg-2">
                                <label>
                                    Return Month:
                                </label>
                                <input type='text' name="inv_return_month" class="form-control"
                                       value="{{ date('m/Y') }}" id="monthyeardate"
                                       readonly placeholder="Select date"/>

                                <span class="m-form__help">
														Return Month
													</span>
                            </div>


                            <div class="col-lg-2">
                                <label>
                                    Due Date:
                                </label>
                                <input type='text' name="inv_due_date" class="form-control" value="{{ date('d/m/Y') }}"
                                       id="m_datepicker_1"
                                       readonly placeholder="Select date"/>

                                <span class="m-form__help">
														Due Date
													</span>
                            </div>
                        </div>


                        <div class="form-group m-form__group row nameform">
                            <div class="col-lg-6">

                                <div class="col-lg-12 ui-widget">
                                    <label>
                                        Customer Name:

                                        <a class="btn btn-danger
                                         m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air deleteaddress"
                                           data-toggle="m-popover" title="Delete"
                                           data-content="Delete Name address and Pan"
                                           style="color: #ffffff"
                                        >
                                            <i class="la la-trash-o"></i>
                                        </a>

                                    </label>
                                    <input type="text"
                                           name="inv_customer_name"
                                           id="customername"
                                           autocomplete="off"
                                           class="form-control m-input--air"
                                           placeholder="Customer Name start typing first 3 Letter">

                                </div>
                                <br>

                                <div class="col-lg-12">

                                    <input type="text" name="inv_customer_gstin"
                                           autocomplete="off"
                                           class="form-control m-input--air gstin"
                                           placeholder="GSTIN">


                                </div>
                                <br>

                                <div class="col-lg-12">

                                    <input type="text" name="inv_customer_state"
                                           autocomplete="off"
                                           class="form-control m-input--air placeofsupply"
                                           placeholder="Place of Supply">


                                </div>


                            </div>
                            <div class="col-lg-3">
                                <label class="">
                                    Billing Address:
                                </label>
                                <div class="alert m-alert m-alert--default billingaddress" role="alert"
                                     style="height: 250px">


                                    <input type="text" name="inv_person_name"
                                           autocomplete="off"
                                           class="form-control m-input--air inv_person_name"
                                           placeholder="Contact Person">
                                    <input type="text" name="inv_street"
                                           autocomplete="off"
                                           class="form-control m-input--air inv_street"
                                           placeholder="Street and Locality">
                                    <input type="text" name="inv_city"
                                           autocomplete="off"
                                           class="form-control m-input--air inv_city"
                                           placeholder="City">
                                    <input type="text" name="inv_state"
                                           class="form-control m-input--air inv_state"
                                           placeholder="State">
                                    <input type="text" name="inv_country"
                                           class="form-control m-input--air inv_country"
                                           placeholder="Country">
                                    <input type="text" name="inv_pin"
                                           class="form-control m-input--air inv_pin"
                                           placeholder="Pin">


                                </div>

                            </div>
                            <div class="col-lg-3">
                                <label>
                                    Shipping Address:
                                </label>
                                <div class="" role="alert" style="height: 150px;">
                                    <div class="">
                                        <textarea
                                                name="inv_customer_bill_address"
                                                class="form-control m-input"
                                                id="shippingaddress"
                                                rows="8"></textarea>
                                    </div>
                                </div>

                            </div>


                        </div>


                        <div class="form-group">

                            <div class="col-lg-12">

                                <div id="alertformerror"></div>


                            </div>
                        </div>


                        <div class="form-group">

                            <div class="col-lg-12">
                                <div id="invoice_table" class="form-group m-form__group row">

                                    <div class="producthead">

                                        <div class="m-stack m-stack--ver m-stack--general  m-stack--demo">
                                            <div class="m-stack__item m-stack__item--center" style="width: 200px;">
                                                Product Name
                                                <input type="text"
                                                       id="proname"
                                                       class="form-control m-input productname productsearch priupdate"
                                                       name="productname[]"
                                                       placeholder="Product Name">
                                            </div>

                                            <div class="m-stack__item">
                                                Item Type

                                                <select class="form-control m-input type">
                                                    <option>
                                                        Goods
                                                    </option>
                                                    <option>
                                                        Service
                                                    </option>
                                                </select>
                                            </div>


                                            <div class="m-stack__item ">
                                                HSN / SAC
                                                <input type="text" readonly
                                                       class="form-control m-input hsn"
                                                       placeholder="HSN / SAC">
                                            </div>
                                            <div class="m-stack__item">
                                                Qty
                                                <div class="input-group m-input-group priupdate">
													<span class="input-group-addon calculate" id="basic-addon1">
														<i class="la la-sort-numeric-asc"></i>
													</span>
                                                    <input type="text"
                                                           class="form-control m-input qty priupdate"
                                                           placeholder="Qty"
                                                           name="invoice_product_qty[]" value="1"
                                                           aria-describedby="basic-addon1"

                                                    >
                                                </div>
                                            </div>
                                            <div class="m-stack__item">
                                                Unit
                                                <div class="input-group m-input-group">
													<span class="input-group-addon" id="basic-addon1">
														<i class="la la-balance-scale"></i>
													</span>
                                                    <input type="text"
                                                           class="form-control m-input unit" readonly
                                                           placeholder="Unit" aria-describedby="basic-addon1">
                                                </div>
                                            </div>
                                            <div class="m-stack__item">
                                                Rate
                                                <div class="input-group m-input-group ">
													<span class="input-group-addon" id="basic-addon1">
														<i class="la la-rupee"></i>
													</span>
                                                    <input type="text" class="form-control
                                                        m-input rate priupdate"
                                                           placeholder="Rate"
                                                           name="invoice_product_price[]"
                                                           id="itm_rate"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                            </div>

                                            <div class="m-stack__item">
                                                Taxable Value
                                                <div class="input-group m-input-group ">
													<span class="input-group-addon" id="basic-addon1">
														<i class="la la-rupee"></i>
													</span>
                                                    <input type="text" class="form-control
                                                        m-input item_total_taxable_rate priupdate"
                                                           placeholder="Taxable Value"
                                                           disabled
                                                           name="item_total_taxable_rate[]"
                                                           id="item_total_taxable_rate"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                            </div>


                                        </div>

                                        <div class="m-stack m-stack--ver m-stack--general  m-stack--demo">
                                            <div class="m-stack__item">
                                                Tax Value

                                                <div class="input-group m-input-group">
													<span class="input-group-addon" id="basic-addon1">
														<i class="la la-rupee"></i>
													</span>
                                                    <input type="text" class="form-control m-input taxval priupdate"
                                                           id="itm_tax_val" readonly
                                                           placeholder="" aria-describedby="basic-addon1">
                                                </div>
                                            </div>
                                            <div class="m-stack__item">
                                                Tax. Rate
                                                <div class="input-group m-input-group">

                                                    <select class="form-control m-input taxper priupdate gstpercentage"
                                                            readonly=""
                                                    >
                                                        <option>
                                                            0
                                                        </option>
                                                        <option>
                                                            0.25
                                                        </option>
                                                        <option>
                                                            3
                                                        </option>
                                                        <option>
                                                            5
                                                        </option>
                                                        <option>
                                                            12
                                                        </option>
                                                        <option>
                                                            18
                                                        </option>
                                                        <option>
                                                            28
                                                        </option>


                                                    </select>

                                                    <span class="input-group-addon" id="basic-addon2">
														%
													</span>
                                                </div>
                                            </div>
                                            <div class="m-stack__item">
                                                CGST
                                                <div class="input-group m-input-group">

                                                    <input type="text" class="form-control m-input cgst priupdate"
                                                           placeholder=""
                                                           aria-describedby="basic-addon1">
                                                    <span class="input-group-addon" id="basic-addon1">
														%
													</span>
                                                </div>
                                            </div>
                                            <div class="m-stack__item">
                                                SGST
                                                <div class="input-group m-input-group">

                                                    <input type="text" class="form-control m-input sgst priupdate"
                                                           placeholder=""
                                                           aria-describedby="basic-addon1">
                                                    <span class="input-group-addon" id="basic-addon1">
														%
													</span>
                                                </div>
                                            </div>
                                            <div class="m-stack__item">
                                                IGST
                                                <div class="input-group m-input-group">

                                                    <input type="text" class="form-control m-input igst priupdate"
                                                           placeholder=""
                                                           aria-describedby="basic-addon1">
                                                    <span class="input-group-addon" id="basic-addon1">
														%
													</span>
                                                </div>
                                            </div>
                                            <div class="m-stack__item">
                                                CESS %
                                                <div class="input-group m-input-group">

                                                    <input type="text" class="form-control m-input cessper"
                                                           placeholder=""
                                                           aria-describedby="basic-addon1">
                                                    <span class="input-group-addon" id="basic-addon1">
														%
													</span>
                                                </div>
                                            </div>
                                            <div class="m-stack__item">
                                                Cess Amt
                                                <div class="input-group m-input-group">
													<span class="input-group-addon" id="basic-addon1">
														<i class="la la-inr"></i>
													</span>
                                                    <input type="text" class="form-control m-input cessamt" readonly
                                                           placeholder="Amt" aria-describedby="basic-addon1">
                                                </div>
                                            </div>
                                            <div class="m-stack__item" style="width: 150px">
                                                Total
                                                <div class="input-group m-input-group">
													<span class="input-group-addon" id="basic-addon1">
														<i class="la la-inr"></i>
													</span>
                                                    <input type="text" readonly
                                                           class="form-control m-input calculate-sub prototal"
                                                           placeholder="Total"
                                                           aria-describedby="basic-addon1"
                                                    >
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12">
                                            <br>
                                            <div
                                                    class="btn btn-outline-success btn-block add-row">
														<span>
															<i class="la la-plus"></i>
															<span>
																Add New Product/ Item
															</span>
														</span>
                                            </div>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>


                        <div class="m-form__group">
                            <div class="m-section__content">
                                <table class="table m-table m-table--head-bg-success"
                                       id="inv_table">
                                    <thead>
                                    <tr>
                                        <th>
                                            #
                                        </th>
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Type
                                        </th>
                                        <th>
                                            HSN
                                        </th>
                                        <th>
                                            Qty
                                        </th>
                                        <th>
                                            Unit
                                        </th>
                                        <th>
                                            Tax. Rate<i class="la la-inr"></i>
                                        </th>
                                        <th>
                                            Rate<i class="la la-inr"></i>
                                        </th>

                                        <th>
                                            Tax<i class="la la-inr"></i>
                                        </th>
                                        <th>
                                            Tax%
                                        </th>
                                        <th>
                                            CGST%
                                        </th>
                                        <th>
                                            SGST%
                                        </th>
                                        <th>
                                            IGST%
                                        </th>
                                        <th>
                                            CESS%
                                        </th>
                                        <th>
                                            CESS<i class="la la-inr"></i>
                                        </th>
                                        <th>
                                            TOTAL<i class="la la-inr"></i>
                                        </th>
                                        <th>
                                            ACTION
                                        </th>


                                    </tr>
                                    </thead>

                                    <tbody>

                                    <tr>
                                        <td>fff</td>
                                        <td>fff</td>
                                        <td>fff</td>
                                        <td>fff</td>
                                        <td>fff</td>
                                        <td>fff</td>
                                        <td>fff</td>
                                        <td>fff</td>
                                        <td>fff</td>
                                        <td>fff</td>
                                        <td>fff</td>
                                        <td>fff</td>
                                        <td>fff</td>
                                        <td>fff</td>
                                        <td>fff</td>
                                        <td>fff</td>
                                        <td>fff</td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>


                    </div>

                    <hr>
                    <div class="m-section__content">
                        <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                            <div class="m-demo__preview">
                                <div class="m-stack m-stack--ver m-stack--general m-stack--demo">
                                    <div class="m-stack__item">

                                        <div class="m-widget1__item">
                                            <div class="row m-row--no-padding align-items-center">
                                                <div class="col">
                                                    <h5 class="m-widget1__title">
                                                        Total
                                                    </h5>
                                                    <span class="m-widget1__desc">
															Total Invoice Value
                                                        <div class="democlass">
<p class="democlass">hhghggh</p>
                                                        </div>
														</span>
                                                </div>
                                                <div class="col m--align-right">
														<span class="m-widget1__number m--font-brand">
															<h3><i class="la la-rupee"></i>
                                                            <spam class="invoice_total_value">
                                                                0
                                                            </spam>
                                                            </h3>
														</span>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="m-stack__item">
                                        <div class="m-widget1__item">
                                            <div class="row m-row--no-padding align-items-center">
                                                <div class="col">
                                                    <h5 class="m-widget1__title">
                                                        Taxable
                                                    </h5>
                                                    <span class="m-widget1__desc">
															Total Taxable Value
														</span>

                                                </div>
                                                <div class="col m--align-right">
														<span class="m-widget1__number m--font-danger">
															<h3><i class="la la-rupee"></i>
                                                            <spam class="invoice_tax_value">
                                                                0
                                                            </spam>
                                                            </h3>
														</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-stack__item">
                                        <div class="m-widget1__item">
                                            <div class="row m-row--no-padding align-items-center">
                                                <div class="col">
                                                    <h5 class="m-widget1__title">
                                                        CGST
                                                    </h5>
                                                    <span class="m-widget1__desc">
															Total CGST Value
														</span>
                                                </div>
                                                <div class="col m--align-right">
														<span class="m-widget1__number m--font-success">
															<h3><i class="la la-rupee"></i>
                                                            <spam class="invoice_cgst_value">
                                                                0
                                                            </spam>
                                                            </h3>
														</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-stack__item">
                                        <div class="m-widget1__item">
                                            <div class="row m-row--no-padding align-items-center">
                                                <div class="col">
                                                    <h5 class="m-widget1__title">
                                                        SGST
                                                    </h5>
                                                    <span class="m-widget1__desc">
															Total SGST Value
														</span>
                                                </div>
                                                <div class="col m--align-right">
														<span class="m-widget1__number m--font-success">
															<h3><i class="la la-rupee"></i>
                                                            <spam class="invoice_sgst_value">
                                                                0
                                                            </spam>
                                                            </h3>
														</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="m-stack__item">
                                        <div class="m-widget1__item">
                                            <div class="row m-row--no-padding align-items-center">
                                                <div class="col">
                                                    <h5 class="m-widget1__title">
                                                        IGST
                                                    </h5>
                                                    <span class="m-widget1__desc">
															Total IGST Value
														</span>
                                                </div>
                                                <div class="col m--align-right">
														<span class="m-widget1__number m--font-success">
															<h3><i class="la la-rupee"></i>
                                                            <spam class="invoice_igst_value">
                                                                0
                                                            </spam>
                                                            </h3>
														</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>

                    <hr>


                    <div class="m-form m-form--fit m-form--label-align-right">
                        <div class="form-group m-form__group row">
                            <div class="row">

                                <div class="col-lg-9">
                                    <a id="invsubmit" class=" btn btn-primary" href="">Submit Invoice</a>
                                </div>

                                <div class="col-lg-3">

                                    <button type="reset" class="btn btn-secondary">
                                        Reset
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>


                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>

    <style>
        input.ui-autocomplete-loading {
            background: white url("loader/ajaxloader.gif") right center no-repeat;
            background-size: 32px 32px;
        }
    </style>


@endsection

@section('client_footer')

    <script src="{{ asset('js/client/sweetalert.min.js') }}"></script>
    {{--auto search--}}
    <link rel="stylesheet" href="{{ asset('css/client/jquery-ui.min.css')}}"/>
    <link rel="stylesheet" href="{{ asset('css/client/jquery-ui.theme.min.css')}}"/>
    <script src="{{ asset('js/client/jquery-ui.min.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            var src = "{{ route('api.get.customer.apisearch') }}";
            $("#customername").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: src,
                        dataType: "json",
                        data: {
                            term: request.term
                        },
                        success: function (data) {
                            response(data);

                        }


                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.value);
                    $('.gstin').val(ui.item.gstin);
                    $('.placeofsupply').val(ui.item.state);
                    $('.inv_person_name').val(ui.item.person_name);
                    $('.inv_street').val(ui.item.street + ui.item.locality);
                    $('.inv_city').val(ui.item.city);
                    $('.inv_state').val(ui.item.state);
                    $('.inv_country').val(ui.item.country);
                    $('.inv_pin').val(ui.item.pin);




                    $('#shippingaddress').val(
                        ui.item.street + "  "
                        + ui.item.locality + "   " + ui.item.city + "  "
                        + ui.item.state + "  " + ui.item.country + "  "
                        + ui.item.pin
                    );


                }

            });
        });
    </script>
    {{--auto search--}}
    <script>
        //== Class definition

        var BootstrapDatepicker = function () {

            //== Private functions
            var demos = function () {
                // minimum setup
                $('#m_datepicker_1, #m_datepicker_1_validate').datepicker({
                    format: "dd-mm-yyyy",
                    todayHighlight: true,
                    orientation: "bottom left",
                    templates: {
                        leftArrow: '<i class="la la-angle-left"></i>',
                        rightArrow: '<i class="la la-angle-right"></i>'
                    }
                });

            };

            return {
                // public functions
                init: function () {
                    demos();
                }
            };
        }();

        jQuery(document).ready(function () {
            BootstrapDatepicker.init();
        });
    </script>
    <script>

        $("#monthyeardate").datepicker({
            format: "mm-yyyy",
            startView: "months",
            minViewMode: "months"
        });
    </script>




    <script>
        $(document).ready(function () {
            var src = "{{ route('api.product.fetch.search') }}";
            $(".productsearch").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: src,
                        dataType: "json",
                        data: {
                            term: request.term
                        },
                        success: function (data) {
                            response(data);

                        }


                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.value);
                    $('.hsn').val(ui.item.hsn);
                    $('.unit').val(ui.item.unit_definition);
                    $('.rate').val(ui.item.opeaning_rate);
                    $('.taxper').val(ui.item.igst);
                    $('.cgst').val(ui.item.cgst);
                    $('.sgst').val(ui.item.sgst);
                    $('.igst').val(ui.item.igst);
                    $('.type').val(ui.item.service_type);
                    $('.item_total_taxable_rate').val(ui.item.opeaning_taxable_rate);

//calculations
                    var gsttax = (ui.item.opeaning_rate - ui.item.opeaning_taxable_rate).toFixed(2);

                    $('.taxval').val(gsttax);
                    $('.prototal').val(ui.item.opeaning_rate);

                }


            });
        });
    </script>



    <script>
        $('.priupdate').on('keyup', function () {
            var quantity = $(".qty").val();
            var taxablerate = $(".item_total_taxable_rate").val();
            var taxper = $(".taxper").val();

            // var taxrate = $(".taxper").find(":selected").text();

            // Update individual price
            var total = 0;
            var totaltaxableamt = ((quantity * taxablerate).toFixed(2));
            $('.taxval').val((totaltaxableamt * taxper / 100).toFixed(2));
            //create total
            var taxable = (taxper / 100) + 1;

            $('.prototal').val((totaltaxableamt * taxable).toFixed(2));


        })
            .trigger('keyup');

        $('.gstpercentage').on('change', function () {
            var per = this.value;
            var unit = $('.qty').val();
            var rate = $('.rate').val();
            var taxper = $('.taxper').val();
            //total
            var totalamt = unit * rate;
            $('.prototal').val(totalamt + totalamt * taxper / 100).toFixed(2);
            $('.taxval').val(totalamt * taxper / 100).toFixed(2);
        });


        $('.item_id').on('change', function () {
            var per = this.value;
            var unit = $('.qty').val();
            var rate = $('.rate').val();
            var taxper = $('.taxper').val();
            //total
            var totalamt = unit * rate;
            $('.prototal').val(totalamt + totalamt * taxper / 100).toFixed(2);
            $('.taxval').val(totalamt * taxper / 100).toFixed(2);
        });


    </script>


    <script>
        $(document).ready(function () {

            // copy customer details to shipping


            var productname = $(".productsearch");
            // add new product row on invoice  inv_table
            //  var cloned = $('#invoice_table').clone();
            $(".add-row").click(function (e) {
                // $(".productname").prop('required',true);
                e.preventDefault();


                //variables


                var initial_invoice_total_value = $('.invoice_total_value').text();
                var thisinputprice = $(".prototal").inputVal();
                var totaltinvoiceprice = Number(initial_invoice_total_value) + Number(thisinputprice);
                var initial_invoice_tax_value = $('.invoice_tax_value').text();
                var thisinputtax = $(".taxval").inputVal();
                var thisproqtyzswrrere = $(".qty").inputVal();
                var thisprotaxavlwersate = $(".item_total_taxable_rate").inputVal();
                var totaltinvoicetax = Number(initial_invoice_tax_value) + (Number(thisinputtax)  );
                var totaltinvoicetaxmain = Number(initial_invoice_tax_value) + (Number(thisprotaxavlwersate) * thisproqtyzswrrere );
                var this_item_total_rate = $(".item_total_taxable_rate").inputVal() * $(".qty").inputVal();
                var initial_iinvoice_cgst_value = $('.invoice_cgst_value').text();
                var thisinputcgst = $(".cgst").inputVal();
                var calcu_cgst = Number(initial_iinvoice_cgst_value) + Number(this_item_total_rate) * ( Number(thisinputcgst / 100) );
                var totaltinvoicecgst = +Number(calcu_cgst.toFixed(2));
                var totaltinvoiceigst = +Number(totaltinvoicecgst) * Number(2);








                // $('#proname').attr('required', true);

                var rowCount = $('#inv_table tr').length;
                var sertype = $(".type").find(":selected").text();
                var finalsertype = $.trim(sertype);
                if ($('.productname').val()) {
                    var $tableBody = $('#inv_table').find("tbody"),
                        //$trLast = $tableBody.find("tr:last"),

                        $trNew = $tableBody.append(
                            "<tr class='productstab'>" +
                            // "<td >" + rowCount  + "</td>" +
                            "<td class='item_id'>"  + rowCount  + "</td>" +
                            "<td class='item_name'>" +   productname.inputVal()  + "</td>" +
                            "<td class='item_type'>"  + finalsertype +  "</td>" +
                            "<td class='item_hsn'>" + $(".hsn").inputVal() + "</td>" +
                            "<td class='item_qty'>"  + $(".qty").inputVal()  + "</td>" +
                            "<td class='item_unit'>" +   $(".unit").inputVal()  + "</td>" +
                            "<td class='taxable_rate'>" + +$(".item_total_taxable_rate").inputVal() + "</td>" +
                            "<td class='item_rate'>" +  + $(".rate").inputVal() + "</td>" +
                            "<td class='item_taxval'>" + $(".taxval").inputVal() + "</td>" +
                            "<td class='item_taxper'>"  + $(".taxper").inputVal() +  "</td>" +
                            "<td class='item_cgst'>"  + $(".cgst").inputVal() + "</td>" +
                            "<td class='item_sgst'>" +  $(".sgst").inputVal() + "</td>" +
                            "<td class='item_igst'>" + $(".igst").inputVal() + "</td>" +
                            "<td class='item_cessper'>" + $(".cessper").inputVal() + "</td>" +
                            "<td class='item_cessamt'>" +  $(".cessamt").inputVal() +  "</td>" +
                            "<td class='item_prototal'>" + $(".prototal").inputVal() + "</td>" +
                            "<td style='display: none'>" + totaltinvoicetaxmain.toFixed(2) + "</td>" +
                            "<td style='display: none'>" + totaltinvoicecgst.toFixed(2) + "</td>" +
                            "<td style='display: none'>" + totaltinvoiceigst.toFixed(2) + "</td>" +


                            "<td><a style='color: #ffffff' class=\"btn btn-danger m-btn m-btn--icon m-btn--icon-only deletetabrow mmm\" id='deletetabrow'>\n" +
                            "<i class=\"la la-trash-o\"></i>\n" + "</td>"

                        );

                    //total price update


                    $('.invoice_total_value').replaceWith('<spam class="invoice_total_value">' + totaltinvoiceprice.toFixed(2) + '</spam>');

                    //total tax update



                    $('.invoice_tax_value').replaceWith('<spam class="invoice_tax_value">' + totaltinvoicetaxmain.toFixed(2) + '</spam>');


                    //total cgst update




                    $('.invoice_cgst_value').replaceWith('<spam class="invoice_cgst_value">' + totaltinvoicecgst.toFixed(2) + '</spam>');


                    //total sgst update
                    $('.invoice_sgst_value').replaceWith('<spam class="invoice_sgst_value">' + totaltinvoicecgst.toFixed(2) + '</spam>');

                    //total igst update


                    $('.invoice_igst_value').replaceWith('<spam class="invoice_igst_value">' + totaltinvoiceigst.toFixed(2) + '</spam>');






                    $('.sno').val(rowCount);
                    $tableBody.after($trNew);

                    $(this).closest('.producthead').find("input[type=text], textarea").val("");
                    $('.qty').val('1');
                    $('.prototal').val('0');

                } else {
                    swal("OOPS!", "Please Search for Product and  try to add!", "error");

                }

                $(".deletetabrow").click(function () {


                    //delete total pricing
                    var thisprototaldelete = $(this).closest('tr').find('td:eq(0)').text();
                    var thisprocgstdelete = $(this).closest('tr').children('td:eq(17)').text();
                    var thisprosgstdelete = $(this).closest('tr').children('td:eq(17)').text();
                    var thisproigstdelete = $(this).closest('tr').children('td:eq(18)').text();
                    var thisprotaxabledelete = $(this).closest('tr').children('td:eq(16)').text();

                    console.log(thisprototaldelete);

                    var deletecalcuthisprototaldelete = Number($('.invoice_total_value').text()) - Number(thisprototaldelete);


                    var deletecalcuthisprocgstdelete = Number($('.invoice_cgst_value').text()) - Number(thisprocgstdelete);
                    var deletecalcuthisprosgstdelete = Number($('.invoice_sgst_value').text()) - Number(thisprosgstdelete);
                    var deletecalcuthisproigstdelete = Number($('.invoice_igst_value').text()) - Number(thisproigstdelete);
                    var deletecalcuthisprotaxabledelete = Number($('.invoice_tax_value').text()) - Number(thisprotaxabledelete);


                    ///dekete

                    $('.invoice_total_value').replaceWith('<spam class="invoice_total_value">' + deletecalcuthisprototaldelete.toFixed(2) + '</spam>');

                    //total tax update


                    $('.invoice_tax_value').replaceWith('<spam class="invoice_tax_value">' + deletecalcuthisprotaxabledelete.toFixed(2) + '</spam>');


                    //total cgst update


                    $('.invoice_cgst_value').replaceWith('<spam class="invoice_cgst_value">' + deletecalcuthisprocgstdelete.toFixed(2) + '</spam>');


                    //total sgst update
                    $('.invoice_sgst_value').replaceWith('<spam class="invoice_sgst_value">' + deletecalcuthisprosgstdelete.toFixed(2) + '</spam>');

                    //total igst update


                    $('.invoice_igst_value').replaceWith('<spam class="invoice_igst_value">' + deletecalcuthisproigstdelete.toFixed(2) + '</spam>');


                    // $(this).closest("tr").remove();
                    $(this).closest('tr').find('td:eq(0)').text().replaceWith('hyttt');
                    swal("Success!", "Product Successfully Delete!", "success");

                });


            });
            $(".deleteaddress").click(function (e) {

                $(this).closest('.nameform').find("input[type=text], textarea").val("");
                $('.billreplace').replaceWith(
                    "<div class='billreplace'>" +
                    "</div>"
                );


            });


        });


    </script>


    <script>
        $('#invsubmit').click( function(e) {
            e.preventDefault();


            var rows = [];
            $('#inv_table tbody tr').each(function(i, n){


                var $row = $(n);


                var calcu_item_tax_cgst =
                    ($row.find('td:eq(6)').text() * ( Number($row.find('td:eq(10)').text() / 100) * $row.find('td:eq(4)').text() )).toFixed(2);

                var calcu_item_tax_sgst = ($row.find('td:eq(6)').text() * ( Number($row.find('td:eq(11)').text() / 100) * $row.find('td:eq(4)').text() )).toFixed(2);
                var calcu_item_tax_igst = ($row.find('td:eq(6)').text() * ( Number($row.find('td:eq(12)').text() / 100) * $row.find('td:eq(4)').text() )).toFixed(2);
                var item_tax_value_calcu = (Number($row.find('td:eq(6)').text()) * Number($row.find('td:eq(4)').text())).toFixed(2);





                rows.push({
                    item_sno: $row.find('td:eq(0)').text(),
                    item_name: $row.find('td:eq(1)').text(),
                    item_type: $row.find('td:eq(2)').text(),
                    item_hsn_sac: $row.find('td:eq(3)').text(),
                    item_qty: $row.find('td:eq(4)').text(),
                    item_unit: $row.find('td:eq(5)').text(),
                    item_rate: $row.find('td:eq(6)').text(),
                    taxable_rate: $row.find('td:eq(7)').text(),
                    unit_item_tax_value: $row.find('td:eq(8)').text(),
                    item_tax_value: item_tax_value_calcu,
                    //all calculation

                    item_tax_cgst: calcu_item_tax_cgst,

                    item_tax_sgst: calcu_item_tax_sgst,

                    item_tax_igst: calcu_item_tax_igst,

                    //end all calculation
                    item_tax_rate_percentage: $row.find('td:eq(9)').text(),
                    item_tax_cgst_percentage: $row.find('td:eq(10)').text(),
                    item_tax_sgst_percentage: $row.find('td:eq(11)').text(),
                    item_tax_igst_percentage: $row.find('td:eq(12)').text(),
                    item_tax_cess_percentage: $row.find('td:eq(13)').text(),
                    item_tax_cess: $row.find('td:eq(14)').text(),
                    item_total: $row.find('td:eq(15)').text()
                });
            });


            var _token = $("input[name='_token']").val();
            var inv_id = $("input[name='inv_id']").val();
            var inv_date_of_purchase = $("input[name='inv_date_of_purchase']").val();
            var inv_return_month = $("input[name='inv_return_month']").val();
            var inv_due_date = $("input[name='inv_due_date']").val();
            var customername = $("input[name='inv_customer_name']").val();
            var inv_customer_gstin = $("input[name='inv_customer_gstin']").val();
            var inv_customer_state = $("input[name='inv_customer_state']").val();
            var inv_person_name = $("input[name='inv_person_name']").val();
            var inv_street = $("input[name='inv_street']").val();
            var inv_locality = $("input[name='inv_locality']").val();
            var inv_city = $("input[name='inv_city']").val();
            var inv_state = $("input[name='inv_state']").val();
            var inv_country = $("input[name='inv_country']").val();
            var inv_pin = $("input[name='inv_pin']").val();


            var inv_customer_bill_address = $("textarea[name='inv_customer_bill_address']").val();
            var inv_total = $(".invoice_total_value").text();
            var inv_total_tax_value = $(".invoice_tax_value").text();
            var inv_total_tax_cgst = $(".invoice_cgst_value").text();
            var inv_total_tax_sgst = $(".invoice_sgst_value").text();
            var inv_total_tax_igst = $(".invoice_igst_value").text();








            var d = {

                _token:_token,
                inv_id:inv_id,
                invoice_id: inv_id,
                inv_date_of_purchase:inv_date_of_purchase,
                inv_return_month:inv_return_month,
                inv_due_date:inv_due_date,
                inv_customer_name: customername,
                inv_customer_gstin:inv_customer_gstin,
                inv_customer_state:inv_customer_state,
                inv_customer_bill_address:inv_customer_bill_address,
                inv_total: inv_total,
                inv_total_tax_value: inv_total_tax_value,
                inv_total_tax_cgst: inv_total_tax_cgst,
                inv_total_tax_igst: inv_total_tax_igst,
                inv_pin: inv_pin,
                inv_country: inv_country,
                inv_state: inv_state,
                inv_city: inv_city,
                inv_locality: inv_locality,
                inv_street: inv_street,
                inv_person_name: inv_person_name,
                inv_total_tax_sgst_value: inv_total_tax_sgst,

                //invoice items
                items: rows

            };

            $.ajax({
                url: "{{route('inv.store.post')}}",
                type:'POST',
                data: d,
                success: function (response) {
                    var saved_inv_id = response.id;
                    if (response.data === 'success') {
                        window.location = "/invoice/" + saved_inv_id + "/details";
                    } else {
                        alert('failed');
                    }
                }
            });


        });
    </script>



@endsection
