@extends('client.structure')
@section('client.title', 'View All Sale')
@section('client.menu.sale', 'm-menu__item--active')
@section('client')
    <link rel="stylesheet" href="{{asset('/css/client/datatable.min.css')}}">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Dashboard
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Sale Invoice
											</span>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												View All Sale
											</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->


        <div class="m-content">
            <div class="row">
                <div class="col-xl-12">
                    <!--begin::Portlet-->
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">View All Sale

                                    </h3>

                                </div>
                            </div>

                            <div class="m-portlet__head-tools">
                                <ul class="m-portlet__nav">
                                    <li class="m-portlet__nav-item">
                                        <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push"
                                             data-dropdown-toggle="hover" aria-expanded="true">
                                            <a href="{{route('product.create')}}"
                                               class="btn btn-outline-primary m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air">
															<span>
																<i class="la la-industry"></i>
																<span>
																	Add New Product
																</span>
															</span>
                                            </a>

                                        </div>
                                    </li>
                                </ul>
                            </div>


                        </div>


                        <div class="m-portlet__body">
                            <!--begin::Section-->
                            <div class="m-section">


                                <div class="m-section__content">
                                    <table class="table table-bordered" id="users-table">
                                        <thead>
                                        <tr>
                                            <th>Inv Id</th>
                                            <th>Name</th>
                                            <th>GSTIN</th>
                                            <th>AMOUNT</th>
                                            <th>CREATED At</th>
                                            <th>Details</th>
                                        </tr>
                                        </thead>
                                    </table>


                                </div>


                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



@section('client_footer')
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script>
        $(function () {
            $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                oLanguage: {sProcessing: '<div class="" style="width: 500px; height:100%; background-color: rgba(255, 255, 255, 0.53); display: inline-block;z-index: 700"><img src="{{asset('/loader/ajaxloader.gif')}}" alt=""> <br><div><h3 style="text-align: center">Loading</h3></div></div>'},

                ajax: '{!! route('api.get.invoice') !!}',
                columns: [
                    {data: 'inv_id', name: 'inv_id'},
                    {data: 'inv_customer_name', name: 'inv_customer_name'},
                    {data: 'inv_customer_gstin', name: 'inv_customer_gstin'},
                    {data: 'inv_total_value', name: 'inv_total_value'},
                    {data: 'updated_at', name: 'updated_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}

                ]
            });

        });
    </script>
@endsection

