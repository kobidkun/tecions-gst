@extends('client.structure')
@section('client.title', 'Create new Sale invoice')
@section('client.menu.sale', 'm-menu__item--active')
@section('client')

    <link rel="stylesheet" href="{{asset('css/client/bootdatatable.min.css')}}">
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Dashboard
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Sale Invoice
											</span>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Create New Sale Incoice
											</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Create Sales Invoice
                            </h3>
                        </div>
                    </div>


                </div>

                <!--begin::Form to add product-->
                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed">
                    {{ csrf_field() }}

                    <div class="m-portlet__body">

                        @if ($errors->any())

                            <div class="form-group m-form__group row">
                                <div class="col-lg-12">


                                    <div class="form-group m-form__group row">

                                        <div class="col-lg-12 col-md-12 col-sm-12">

                                            <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger alert-dismissible fade show"
                                                 role="alert">
                                                <div class="m-alert__icon">
                                                    <i class="flaticon-exclamation-1"></i>
                                                    <span></span>
                                                </div>
                                                <div class="m-alert__text">
                                                    <strong>
                                                        OOPS! <br>
                                                    </strong>
                                                    @foreach ($errors->all() as $error)
                                                        {{ $error }} <br>

                                                    @endforeach
                                                </div>
                                                <div class="m-alert__close">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                            aria-label="Close"></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>

                        @endif

                        <div class="form-group m-form__group row">
                            <div class="col-lg-4">
                                <label>
                                    Invoice Id:
                                </label>
                                <input type="text" name="inv_id"
                                       autocomplete="off"
                                       value="{{$invid->inv_id +1}}"
                                       class="form-control m-input--air"
                                       placeholder="Required">
                                <span class="m-form__help">
														Invoice Id
													</span>
                            </div>
                            <div class="col-lg-4">
                                <label class="">
                                    Date of Invoice:
                                </label>
                                <input type='text' name="inv_date_of_purchase" class="form-control"
                                       value="{{ date('d/m/Y') }}" id="m_datepicker_1"
                                       readonly placeholder="Select date"/>

                                <span class="m-form__help">
														Date of invoice
													</span>
                            </div>
                            <div class="col-lg-2">
                                <label>
                                    Return Month:
                                </label>
                                <input type='text' name="inv_return_month" class="form-control"
                                       value="{{ date('m/Y') }}" id="monthyeardate"
                                       readonly placeholder="Select date"/>

                                <span class="m-form__help">
														Return Month
													</span>
                            </div>


                            <div class="col-lg-2">
                                <label>
                                    Due Date:
                                </label>
                                <input type='text' name="inv_due_date" class="form-control" value="{{ date('d/m/Y') }}"
                                       id="m_datepicker_1"
                                       readonly placeholder="Select date"/>

                                <span class="m-form__help">
														Due Date
													</span>
                            </div>
                        </div>


                        <div class="form-group m-form__group row nameform">
                            <div class="col-lg-6">

                                <div class="col-lg-12 ui-widget">
                                    <label>
                                        Customer Name:

                                        <a class="btn btn-danger
                                         m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air deleteaddress"
                                           data-toggle="m-popover" title="Delete"
                                           data-content="Delete Name address and Pan"
                                           style="color: #ffffff"
                                        >
                                            <i class="la la-trash-o"></i>
                                        </a>

                                    </label>
                                    <input type="text"
                                           name="inv_customer_name"
                                           id="customername"
                                           autocomplete="off"
                                           class="form-control m-input--air"
                                           placeholder="Customer Name start typing first 3 Letter">

                                </div>
                                <br>

                                <div class="col-lg-12">

                                    <input type="text" name="inv_customer_gstin"
                                           autocomplete="off"
                                           class="form-control m-input--air gstin"
                                           placeholder="GSTIN">


                                </div>
                                <br>

                                <div class="col-lg-12">

                                    <input type="text" name="inv_customer_state"
                                           autocomplete="off"
                                           class="form-control m-input--air placeofsupply"
                                           placeholder="Place of Supply">


                                </div>


                            </div>
                            <div class="col-lg-3">
                                <label class="">
                                    Billing Address:
                                </label>
                                <div class="alert m-alert m-alert--default billingaddress" role="alert"
                                     style="height: 250px">


                                    <input type="text" name="inv_person_name"
                                           autocomplete="off"
                                           class="form-control m-input--air inv_person_name"
                                           placeholder="Contact Person">
                                    <input type="text" name="inv_street"
                                           autocomplete="off"
                                           class="form-control m-input--air inv_street"
                                           placeholder="Street and Locality">
                                    <input type="text" name="inv_city"
                                           autocomplete="off"
                                           class="form-control m-input--air inv_city"
                                           placeholder="City">
                                    <input type="text" name="inv_state"
                                           class="form-control m-input--air inv_state"
                                           placeholder="State">
                                    <input type="text" name="inv_country"
                                           class="form-control m-input--air inv_country"
                                           placeholder="Country">
                                    <input type="text" name="inv_pin"
                                           class="form-control m-input--air inv_pin"
                                           placeholder="Pin">


                                </div>

                            </div>
                            <div class="col-lg-3">
                                <label>
                                    Shipping Address:
                                </label>
                                <div class="" role="alert" style="height: 150px;">
                                    <div class="">
                                        <textarea
                                                name="inv_customer_bill_address"
                                                class="form-control m-input"
                                                id="shippingaddress"
                                                rows="8"></textarea>
                                    </div>
                                </div>

                            </div>


                        </div>


                        <div class="form-group">

                            <div class="col-lg-12">

                                <div id="alertformerror"></div>


                            </div>
                        </div>


                        <div class="form-group">

                            <div class="col-lg-12">
                                <div id="invoice_table" class="form-group m-form__group row">

                                    <div class="producthead">

                                        <div class="m-stack m-stack--ver m-stack--general  m-stack--demo">
                                            <div class="m-stack__item m-stack__item--center" style="width: 200px;">
                                                Product Name
                                                <input type="text"
                                                       id="proname"
                                                       class="form-control m-input productname productsearch priupdate"
                                                       name="productname[]"
                                                       placeholder="Product Name">
                                            </div>

                                            <div class="m-stack__item">
                                                Item Type

                                                <select class="form-control m-input type">
                                                    <option>
                                                        Goods
                                                    </option>
                                                    <option>
                                                        Service
                                                    </option>
                                                </select>
                                            </div>


                                            <div class="m-stack__item ">
                                                HSN / SAC
                                                <input type="text" readonly
                                                       class="form-control m-input hsn"
                                                       placeholder="HSN / SAC">
                                            </div>
                                            <div class="m-stack__item">
                                                Qty
                                                <div class="input-group m-input-group priupdate">
													<span class="input-group-addon calculate" id="basic-addon1">
														<i class="la la-sort-numeric-asc"></i>
													</span>
                                                    <input type="text"
                                                           class="form-control m-input qty priupdate"
                                                           placeholder="Qty"
                                                           name="invoice_product_qty[]" value="1"
                                                           aria-describedby="basic-addon1"

                                                    >
                                                </div>
                                            </div>
                                            <div class="m-stack__item">
                                                Unit
                                                <div class="input-group m-input-group">
													<span class="input-group-addon" id="basic-addon1">
														<i class="la la-balance-scale"></i>
													</span>
                                                    <input type="text"
                                                           class="form-control m-input unit" readonly
                                                           placeholder="Unit" aria-describedby="basic-addon1">
                                                </div>
                                            </div>
                                            <div class="m-stack__item">
                                                Rate
                                                <div class="input-group m-input-group ">
													<span class="input-group-addon" id="basic-addon1">
														<i class="la la-rupee"></i>
													</span>
                                                    <input type="text" class="form-control
                                                        m-input rate priupdate"
                                                           placeholder="Rate"
                                                           name="invoice_product_price[]"
                                                           id="itm_rate"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                            </div>

                                            <div class="m-stack__item">
                                                Taxable Value
                                                <div class="input-group m-input-group ">
													<span class="input-group-addon" id="basic-addon1">
														<i class="la la-rupee"></i>
													</span>
                                                    <input type="text" class="form-control
                                                        m-input item_total_taxable_rate priupdate"
                                                           placeholder="Taxable Value"
                                                           disabled
                                                           name="item_total_taxable_rate[]"
                                                           id="item_total_taxable_rate"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                            </div>


                                        </div>

                                        <div class="m-stack m-stack--ver m-stack--general  m-stack--demo">
                                            <div class="m-stack__item">
                                                Tax Value

                                                <div class="input-group m-input-group">
													<span class="input-group-addon" id="basic-addon1">
														<i class="la la-rupee"></i>
													</span>
                                                    <input type="text" class="form-control m-input taxval priupdate"
                                                           id="itm_tax_val" readonly
                                                           placeholder="" aria-describedby="basic-addon1">
                                                </div>
                                            </div>
                                            <div class="m-stack__item">
                                                Tax. Rate
                                                <div class="input-group m-input-group">

                                                    <select class="form-control m-input taxper priupdate gstpercentage"
                                                            readonly=""
                                                    >
                                                        <option>
                                                            0
                                                        </option>
                                                        <option>
                                                            0.25
                                                        </option>
                                                        <option>
                                                            3
                                                        </option>
                                                        <option>
                                                            5
                                                        </option>
                                                        <option>
                                                            12
                                                        </option>
                                                        <option>
                                                            18
                                                        </option>
                                                        <option>
                                                            28
                                                        </option>


                                                    </select>

                                                    <span class="input-group-addon" id="basic-addon2">
														%
													</span>
                                                </div>
                                            </div>
                                            <div class="m-stack__item">
                                                CGST
                                                <div class="input-group m-input-group">

                                                    <input type="text" class="form-control m-input cgst priupdate"
                                                           placeholder=""
                                                           aria-describedby="basic-addon1">
                                                    <span class="input-group-addon" id="basic-addon1">
														%
													</span>
                                                </div>
                                            </div>
                                            <div class="m-stack__item">
                                                SGST
                                                <div class="input-group m-input-group">

                                                    <input type="text" class="form-control m-input sgst priupdate"
                                                           placeholder=""
                                                           aria-describedby="basic-addon1">
                                                    <span class="input-group-addon" id="basic-addon1">
														%
													</span>
                                                </div>
                                            </div>
                                            <div class="m-stack__item">
                                                IGST
                                                <div class="input-group m-input-group">

                                                    <input type="text" class="form-control m-input igst priupdate"
                                                           placeholder=""
                                                           aria-describedby="basic-addon1">
                                                    <span class="input-group-addon" id="basic-addon1">
														%
													</span>
                                                </div>
                                            </div>
                                            <div class="m-stack__item">
                                                CESS %
                                                <div class="input-group m-input-group">

                                                    <input type="text" class="form-control m-input cessper"
                                                           placeholder=""
                                                           aria-describedby="basic-addon1">
                                                    <span class="input-group-addon" id="basic-addon1">
														%
													</span>
                                                </div>
                                            </div>
                                            <div class="m-stack__item">
                                                Cess Amt
                                                <div class="input-group m-input-group">
													<span class="input-group-addon" id="basic-addon1">
														<i class="la la-inr"></i>
													</span>
                                                    <input type="text" class="form-control m-input cessamt" readonly
                                                           placeholder="Amt" aria-describedby="basic-addon1">
                                                </div>
                                            </div>
                                            <div class="m-stack__item" style="width: 150px">
                                                Total
                                                <div class="input-group m-input-group">
													<span class="input-group-addon" id="basic-addon1">
														<i class="la la-inr"></i>
													</span>
                                                    <input type="text" readonly
                                                           class="form-control m-input calculate-sub prototal"
                                                           placeholder="Total"
                                                           aria-describedby="basic-addon1"
                                                    >
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12">
                                            <br>
                                            <div
                                                    class="btn btn-outline-success btn-block add-row">
														<span>
															<i class="la la-plus"></i>
															<span>
																Add New Product/ Item
															</span>
														</span>
                                            </div>
                                        </div>

                                    </div>


                                </div>
                            </div>
                        </div>


                        <div class="m-form__group">
                            <div class="m-section__content">
                                {{-- <table class="table m-table m-table--head-bg-success"
                                        id="inv_table">
                                     <thead>
                                     <tr>
                                         <th>
                                             #
                                         </th>
                                         <th>
                                             Name
                                         </th>
                                         <th>
                                             Type
                                         </th>
                                         <th>
                                             HSN
                                         </th>
                                         <th>
                                             Qty
                                         </th>
                                         <th>
                                             Unit
                                         </th>
                                         <th>
                                             Tax. Rate<i class="la la-inr"></i>
                                         </th>
                                         <th>
                                             Rate<i class="la la-inr"></i>
                                         </th>

                                         <th>
                                             Tax<i class="la la-inr"></i>
                                         </th>
                                         <th>
                                             Tax%
                                         </th>
                                         <th>
                                             CGST%
                                         </th>
                                         <th>
                                             SGST%
                                         </th>
                                         <th>
                                             IGST%
                                         </th>
                                         <th>
                                             CESS%
                                         </th>
                                         <th>
                                             CESS<i class="la la-inr"></i>
                                         </th>
                                         <th>
                                             TOTAL<i class="la la-inr"></i>
                                         </th>
                                         <th>
                                             ACTION
                                         </th>


                                     </tr>
                                     </thead>

                                     <tbody>

                                     <tr>
                                         <td>fff</td>
                                         <td>fff</td>
                                         <td>fff</td>
                                         <td>fff</td>
                                         <td>fff</td>
                                         <td>fff</td>
                                         <td>fff</td>
                                         <td>fff</td>
                                         <td>fff</td>
                                         <td>fff</td>
                                         <td>fff</td>
                                         <td>fff</td>
                                         <td>fff</td>
                                         <td>fff</td>
                                         <td>fff</td>
                                         <td>fff</td>
                                         <td>fff</td>
                                     </tr>

                                     </tbody>
                                 </table>--}}

                                <style>

                                    .background {
                                        position: relative;
                                        text-align: center;
                                    }

                                    .background span {
                                        background: #fff;
                                        padding: 0 15px;
                                        position: relative;
                                        z-index: 1;
                                    }

                                    .background:before {
                                        background: #ddd;
                                        content: "";
                                        display: block;
                                        height: 1px;
                                        position: absolute;
                                        top: 50%;
                                        width: 100%;
                                    }

                                    .background:before {
                                        left: 0;
                                    }


                                </style>

                                <table id="example" class="table  table-bordered table-responsive" cellspacing="0"
                                       width="100%">
                                    <thead style="width:100%">
                                    <tr>

                                        <th colspan="4">
                                            <div class="background"><span>Info</span></div>
                                        </th>
                                        <th colspan="2">
                                            <div class="background"><span>Details</span></div>
                                        </th>
                                        <th colspan="2">
                                            <div class="background"><span>Rate</span></div>
                                        </th>
                                        <th colspan="10">
                                            <div class="background"><span>Tax Description</span></div>
                                        </th>
                                        <th colspan="2">
                                            <div class="background"><span>Total</span></div>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>A</th>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Type</th>
                                        <th>HSN</th>
                                        <th>Qty</th>
                                        <th>Unit</th>
                                        <th style="background: #5A00DC; color: #ffffff; font-weight: 500">TOTAL</th>
                                        <th style="background: #5A00DC; color: #ffffff; font-weight: 500">T.AMT</th>
                                        <th style="background: #5A00DC; color: #ffffff; font-weight: 500">T.Tax</th>
                                        <th>TRate</th>
                                        <th>Rate</th>
                                        <th>CGST%</th>
                                        <th>SGST%</th>
                                        <th>IGST%</th>
                                        <th style="background: #5A00DC; color: #ffffff; font-weight: 500">SGST</th>
                                        <th style="background: #5A00DC; color: #ffffff; font-weight: 500">CGST</th>
                                        <th style="background: #5A00DC; color: #ffffff; font-weight: 500">IGST</th>
                                        <th>Tax%</th>
                                        <th>CESS%</th>
                                        <th>CESS</th>

                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th style="border-right: none"></th>
                                        <th style="border: none"></th>
                                        <th style="border: none"></th>
                                        <th style="border: none"></th>
                                        <th style="border: none"></th>
                                        <th></th>
                                        <th>Total:</th>
                                        <th style="background: #5A00DC; color: #ffffff;font-weight: 700"></th>
                                        <th style="background: #5A00DC; color: #ffffff;font-weight: 700"></th>
                                        <th style="background: #5A00DC; color: #ffffff;font-weight: 700"></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th style="background: #5A00DC; color: #ffffff; font-weight: 500"></th>
                                        <th style="background: #5A00DC; color: #ffffff;font-weight: 700"></th>
                                        <th style="background: #5A00DC; color: #ffffff;font-weight: 700"></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                </table>


                            </div>
                        </div>


                    </div>

                    <hr>


                    <div class="m-form m-form--fit m-form--label-align-right">
                        <div class="form-group m-form__group row">
                            <div class="row">

                                <div class="col-lg-9">
                                    <a id="invsubmit" class=" btn btn-primary" href="">Submit Invoice</a>
                                </div>

                                <div class="col-lg-3">

                                    <button type="reset" class="btn btn-secondary">
                                        Reset
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>


                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>

    <style>
        input.ui-autocomplete-loading {
            background: white url("../loader/ajaxloader.gif") right center no-repeat;
            background-size: 32px 32px;
        }
    </style>


@endsection

@section('client_footer')
    <script src="{{asset('js/client/datatable.min.js')}}"></script>
    <script src="{{asset('js/client/dataTables.fixedColumns.min.js')}}"></script>
    <script src="{{ asset('js/client/sweetalert.min.js') }}"></script>
    {{--auto search--}}
    <link rel="stylesheet" href="{{ asset('css/client/jquery-ui.min.css')}}"/>
    <link rel="stylesheet" href="{{ asset('css/client/jquery-ui.theme.min.css')}}"/>
    <script src="{{ asset('js/client/jquery-ui.min.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            var src = "{{ route('api.get.customer.apisearch') }}";
            $("#customername").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: src,
                        dataType: "json",
                        data: {
                            term: request.term
                        },
                        success: function (data) {
                            response(data);

                        }


                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.value);
                    $('.gstin').val(ui.item.gstin);
                    $('.placeofsupply').val(ui.item.state);
                    $('.inv_person_name').val(ui.item.person_name);
                    $('.inv_street').val(ui.item.street + ui.item.locality);
                    $('.inv_city').val(ui.item.city);
                    $('.inv_state').val(ui.item.state);
                    $('.inv_country').val(ui.item.country);
                    $('.inv_pin').val(ui.item.pin);


                    $('#shippingaddress').val(
                        ui.item.street + "  "
                        + ui.item.locality + "   " + ui.item.city + "  "
                        + ui.item.state + "  " + ui.item.country + "  "
                        + ui.item.pin
                    );


                }

            });
        });
    </script>
    {{--auto search--}}
    <script>
        //== Class definition

        var BootstrapDatepicker = function () {

            //== Private functions
            var demos = function () {
                // minimum setup
                $('#m_datepicker_1, #m_datepicker_1_validate').datepicker({
                    format: "dd-mm-yyyy",
                    todayHighlight: true,
                    orientation: "bottom left",
                    templates: {
                        leftArrow: '<i class="la la-angle-left"></i>',
                        rightArrow: '<i class="la la-angle-right"></i>'
                    }
                });

            };

            return {
                // public functions
                init: function () {
                    demos();
                }
            };
        }();

        jQuery(document).ready(function () {
            BootstrapDatepicker.init();
        });
    </script>
    <script>

        $("#monthyeardate").datepicker({
            format: "mm-yyyy",
            startView: "months",
            minViewMode: "months"
        });
    </script>
    <script>
        $(document).ready(function () {
            var src = "{{ route('api.product.fetch.search') }}";
            $(".productsearch").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: src,
                        dataType: "json",
                        data: {
                            term: request.term
                        },
                        success: function (data) {
                            response(data);

                        }


                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.value);
                    $('.hsn').val(ui.item.hsn);
                    $('.unit').val(ui.item.unit_definition);
                    $('.rate').val(ui.item.opeaning_rate);
                    $('.taxper').val(ui.item.igst);
                    $('.cgst').val(ui.item.cgst);
                    $('.sgst').val(ui.item.sgst);
                    $('.igst').val(ui.item.igst);
                    $('.type').val(ui.item.service_type);
                    $('.item_total_taxable_rate').val(ui.item.opeaning_taxable_rate);

//calculations
                    var gsttax = (ui.item.opeaning_rate - ui.item.opeaning_taxable_rate).toFixed(2);

                    $('.taxval').val(gsttax);
                    $('.prototal').val(ui.item.opeaning_rate);

                }


            });
        });
    </script>
    <script>
        $('.priupdate').on('keyup', function () {
            var quantity = $(".qty").val();
            var taxablerate = $(".item_total_taxable_rate").val();
            var taxper = $(".taxper").val();

            // var taxrate = $(".taxper").find(":selected").text();

            // Update individual price
            var total = 0;
            var totaltaxableamt = ((quantity * taxablerate).toFixed(2));
            $('.taxval').val((totaltaxableamt * taxper / 100).toFixed(2));
            //create total
            var taxable = (taxper / 100) + 1;

            $('.prototal').val((totaltaxableamt * taxable).toFixed(2));


        })
            .trigger('keyup');

        $('.gstpercentage').on('change', function () {
            var per = this.value;
            var unit = $('.qty').val();
            var rate = $('.rate').val();
            var taxper = $('.taxper').val();
            //total
            var totalamt = unit * rate;
            $('.prototal').val(totalamt + totalamt * taxper / 100).toFixed(2);
            $('.taxval').val(totalamt * taxper / 100).toFixed(2);
        });


        $('.item_id').on('change', function () {
            var per = this.value;
            var unit = $('.qty').val();
            var rate = $('.rate').val();
            var taxper = $('.taxper').val();
            //total
            var totalamt = unit * rate;
            $('.prototal').val(totalamt + totalamt * taxper / 100).toFixed(2);
            $('.taxval').val(totalamt * taxper / 100).toFixed(2);
        });


    </script>
    <script>
        $(document).ready(function () {

            // copy customer details to shipping
            var t = $('#example').DataTable({
                "footerCallback": function (row, data, start, end, display) {
                    var api = this.api(), data;

                    // Remove the formatting to get integer data for summation
                    var intVal = function (i) {
                        return typeof i === 'string' ?
                            i.replace(/[\$,]/g, '') * 1 :
                            typeof i === 'number' ?
                                i : 0;
                    };

                    // Total over all pages
                    prodtabletotal = api
                        .column(7)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);
                    prodtabletotaltaxonly = api
                        .column(9)
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Total over this page
                    taxtotaltable = api
                        .column(8, {page: 'current'})
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    taxtotaltablecgst = api
                        .column(15, {page: 'current'})
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    taxtotaltablesgst = api
                        .column(16, {page: 'current'})
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    taxtotaltableigst = api
                        .column(17, {page: 'current'})
                        .data()
                        .reduce(function (a, b) {
                            return intVal(a) + intVal(b);
                        }, 0);

                    // Update footer
                    $(api.column(7).footer()).html(
                        '<i class="la la-inr"></i>' + '<span class="invoice_total_value">' + prodtabletotal.toFixed(1) + '</span>'
                    );

                    $(api.column(8).footer()).html(
                        '<i class="la la-inr"></i>' + '<span class="invoice_tax_value">' + taxtotaltable.toFixed(2) + '</span>'
                    );

                    $(api.column(9).footer()).html(
                        '<i class="la la-inr"></i> ' + '<span class="invoice_tax_value">' + prodtabletotaltaxonly.toFixed(2) + '</span>'
                    );

                    $(api.column(15).footer()).html(
                        '<i class="la la-inr"></i> ' + '<span class="invoice_cgst_value">' + taxtotaltablecgst.toFixed(2) + '</span>'
                    );

                    $(api.column(16).footer()).html(
                        '<i class="la la-inr"></i> ' + '<span class="invoice_sgst_value">' + taxtotaltablesgst.toFixed(2) + '</span>'
                    );

                    $(api.column(17).footer()).html(
                        '<i class="la la-inr"></i> ' + '<span class="invoice_igst_value">' + taxtotaltableigst.toFixed(2) + '</span>'
                    );
                },
                oLanguage: {sProcessing: '<div class="m-loader m-loader--primary m-loader--lg" style="width: 500px; display: inline-block;z-index: 700"></div>'},
                "searching": false,
                "lengthChange": false,

                scrollX: true,
                scrollCollapse: true,
                paging: false,


            });


            var productname = $(".productsearch");
            // add new product row on invoice  inv_table
            //  var cloned = $('#invoice_table').clone();


            $('.add-row').on('click', function (e) {
                if ($('.productname').val()) {

                    var counter = 1;
                    var productname = $(".productsearch").inputVal();
                    var initial_invoice_total_value = $('.invoice_total_value').text();
                    var thisinputprice = $(".prototal").inputVal();
                    var totaltinvoiceprice = Number(initial_invoice_total_value) + Number(thisinputprice);
                    var initial_invoice_tax_value = $('.invoice_tax_value').text();
                    var thisinputtax = $(".taxval").inputVal();
                    var thisproqtyzswrrere = $(".qty").inputVal();
                    var thisprotaxavlwersate = $(".item_total_taxable_rate").inputVal();
                    var totaltinvoicetax = Number(initial_invoice_tax_value) + (Number(thisinputtax)  );
                    var totaltinvoicetaxmain = (Number(thisprotaxavlwersate) * thisproqtyzswrrere );
                    var this_item_total_rate = $(".item_total_taxable_rate").inputVal() * $(".qty").inputVal();
                    var initial_iinvoice_cgst_value = $('.invoice_cgst_value').text();
                    var thisinputcgst = $(".cgst").inputVal();
                    var calcu_cgst = Number(initial_iinvoice_cgst_value) + Number(this_item_total_rate) * ( Number(thisinputcgst / 100) );
                    var totaltinvoicecgst = +Number(calcu_cgst.toFixed(2));
                    var totaltinvoicetotaxtax = +Number(totaltinvoicecgst) * Number(2);
                    var rowCount = $('#example tbody tr').length;
                    var sertype = $(".type").find(":selected").text();
                    var finalsertype = $.trim(sertype);
                    var totalultimastetotal = $(".prototal").inputVal();
                    var totaltinvoiceigst = totaltinvoicetotaxtax;
                    e.preventDefault();
                    t.row.add([
                        '<a class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only m-btn--pill m-btn--air deletedatainvoice" data-toggle="m-popover" title="Delete" data-content="Delete Name address and Pan" style="color: #ffffff" > <i class="la la-trash-o"></i> </a>',
                        counter,
                        productname,
                        finalsertype,
                        $(".hsn").inputVal(),
                        $(".qty").inputVal(),
                        $(".unit").inputVal(),
                        totalultimastetotal,
                        totaltinvoicetaxmain.toFixed(2),
                        totaltinvoicetotaxtax.toFixed(2),
                        $(".item_total_taxable_rate").inputVal(),
                        $(".rate").inputVal(),
                        $(".cgst").inputVal(),
                        $(".sgst").inputVal(),
                        $(".igst").inputVal(),
                        totaltinvoicecgst.toFixed(2),
                        totaltinvoicecgst.toFixed(2),
                        totaltinvoiceigst.toFixed(2),
                        $(".taxper").inputVal(),

                        $(".cessper").inputVal(),
                        $(".cessamt").inputVal(),


                    ]).draw(false);


                    $(this).closest('.producthead').find("input[type=text], textarea").val("");

                    $('.prototal').val('0');
                    $('.qty').val('1');
                    $('.prototal').val('0');
                    counter++;
                } else {
                    swal("OOPS!", "Please Search for Product and  try to add!", "error");

                }


            });


            var table = $('#example').DataTable();

            $('#example tbody').on('click', 'a.deletedatainvoice', function () {
                table
                    .row($(this).parents('tr'))
                    .remove()
                    .draw();
            });

            $(".deleteaddress").click(function (e) {

                $(this).closest('.nameform').find("input[type=text], textarea").val("");
                $('.billreplace').replaceWith(
                    "<div class='billreplace'>" +
                    "</div>"
                );


            })

            //footer callback


        });


    </script>
    <script>
        $('#invsubmit').click(function (e) {
            e.preventDefault();
            var customernamevalueexists = $.trim($("#customername").val());
            var tablebodyvalueexists = $("#example tbody tr td").hasClass("sorting_1");
//  sorting_1
            if (customernamevalueexists !== '' && tablebodyvalueexists) {


                var rows = [];
                $('#example tbody tr').each(function (i, n) {


                    var $row = $(n);


                    var calcu_item_tax_cgst =
                        ($row.find('td:eq(6)').text() * ( Number($row.find('td:eq(10)').text() / 100) * $row.find('td:eq(4)').text() )).toFixed(2);

                    var calcu_item_tax_sgst = ($row.find('td:eq(6)').text() * ( Number($row.find('td:eq(11)').text() / 100) * $row.find('td:eq(4)').text() )).toFixed(2);
                    var calcu_item_tax_igst = ($row.find('td:eq(6)').text() * ( Number($row.find('td:eq(12)').text() / 100) * $row.find('td:eq(4)').text() )).toFixed(2);
                    var item_tax_value_calcu = (Number($row.find('td:eq(6)').text()) * Number($row.find('td:eq(4)').text())).toFixed(2);


                    rows.push({
                        item_sno: $row.find('td:eq(1)').text(),
                        item_name: $row.find('td:eq(2)').text(),
                        item_type: $row.find('td:eq(3)').text(),
                        item_hsn_sac: $row.find('td:eq(4)').text(),
                        item_qty: $row.find('td:eq(5)').text(),
                        item_unit: $row.find('td:eq(6)').text(),
                        item_rate: $row.find('td:eq(10)').text(),
                        taxable_rate: $row.find('td:eq(8)').text(),
                        unit_item_tax_value: $row.find('td:eq(10)').text(),
                        item_tax_value: $row.find('td:eq(9)').text(),
                        //all calculation

                        item_tax_cgst: $row.find('td:eq(15)').text(),

                        item_tax_sgst: $row.find('td:eq(16)').text(),

                        item_tax_igst: $row.find('td:eq(17)').text(),

                        //end all calculation
                        item_tax_rate_percentage: $row.find('td:eq(18)').text(),
                        item_tax_cgst_percentage: $row.find('td:eq(12)').text(),
                        item_tax_sgst_percentage: $row.find('td:eq(13)').text(),
                        item_tax_igst_percentage: $row.find('td:eq(14)').text(),
                        item_tax_cess_percentage: $row.find('td:eq(19)').text(),
                        item_tax_cess: $row.find('td:eq(20)').text(),
                        item_total: $row.find('td:eq(7)').text()
                    });
                });


                var _token = $("input[name='_token']").val();
                var inv_id = $("input[name='inv_id']").val();
                var inv_date_of_purchase = $("input[name='inv_date_of_purchase']").val();
                var inv_return_month = $("input[name='inv_return_month']").val();
                var inv_due_date = $("input[name='inv_due_date']").val();
                var customername = $("input[name='inv_customer_name']").val();
                var inv_customer_gstin = $("input[name='inv_customer_gstin']").val();
                var inv_customer_state = $("input[name='inv_customer_state']").val();
                var inv_person_name = $("input[name='inv_person_name']").val();
                var inv_street = $("input[name='inv_street']").val();
                var inv_locality = $("input[name='inv_locality']").val();
                var inv_city = $("input[name='inv_city']").val();
                var inv_state = $("input[name='inv_state']").val();
                var inv_country = $("input[name='inv_country']").val();
                var inv_pin = $("input[name='inv_pin']").val();


                var inv_customer_bill_address = $("textarea[name='inv_customer_bill_address']").val();
                var inv_total = $(".invoice_total_value").text();
                var inv_total_tax_value = $(".invoice_tax_value").text();
                var inv_total_tax_cgst = $(".invoice_cgst_value").text();
                var inv_total_tax_sgst = $(".invoice_sgst_value").text();
                var inv_total_tax_igst = $(".invoice_igst_value").text();


                var d = {

                    _token: _token,
                    inv_id: inv_id,
                    invoice_id: inv_id,
                    inv_date_of_purchase: inv_date_of_purchase,
                    inv_return_month: inv_return_month,
                    inv_due_date: inv_due_date,
                    inv_customer_name: customername,
                    inv_customer_gstin: inv_customer_gstin,
                    inv_customer_state: inv_customer_state,
                    inv_customer_bill_address: inv_customer_bill_address,
                    inv_total: inv_total,
                    inv_total_tax_value: inv_total_tax_value,
                    inv_total_tax_cgst: inv_total_tax_cgst,
                    inv_total_tax_igst: inv_total_tax_igst,
                    inv_pin: inv_pin,
                    inv_country: inv_country,
                    inv_state: inv_state,
                    inv_city: inv_city,
                    inv_locality: inv_locality,
                    inv_street: inv_street,
                    inv_person_name: inv_person_name,
                    inv_total_tax_sgst_value: inv_total_tax_sgst,

                    //invoice items
                    items: rows

                };

                $.ajax({
                    url: "{{route('inv.store.post')}}",
                    type: 'POST',
                    data: d,
                    success: function (response) {
                        var saved_inv_id = response.id;
                        if (response.data === 'success') {
                            window.location = "/invoice/" + saved_inv_id + "/details";
                        } else {
                            alert('failed');
                        }
                    }
                });


                // });
            } else {
                if (customernamevalueexists === '' && !tablebodyvalueexists) {
                    swal("OOPS!", "Please Add  Item to Table and Customer name ", "error");
                }
                else if (!tablebodyvalueexists) {
                    swal("OOPS!", "Please Add  Item to Table", "error");
                } else {
                    swal("OOPS!", "Please Add  Customer name", "error");
                }
            }
        });
    </script>



@endsection

