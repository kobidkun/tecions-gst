@extends('client.structure')
@section('client.title', 'Details of invoice')
@section('client.menu.sale', 'm-menu__item--active')
@section('client')

    <style>
        .invoice-title h2, .invoice-title h3 {
            display: inline-block;
        }

        .table > tbody > tr > .no-line {
            border-top: none;
        }

        .table > thead > tr > .no-line {
            border-bottom: none;
        }

        .table > tbody > tr > .thick-line {
            border-top: 2px solid;
        }
    </style>
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Dashboard
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Sale Invoice
											</span>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Details of invoice
											</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Details of invoice
                            </h3>
                        </div>
                    </div>


                </div>

                <!--begin::Form to add product-->


                <div id="printable" class="m-content">
                    <!--begin::Portlet-->
                    <div class="m-portlet" style="padding: 25px">
                        <div class="col-xs-12">
                            <div class="invoice-title">
                                <h2>Invoice</h2>
                                <h3 class="pull-right">Order # {{$i->inv_id}}</h3>
                            </div>
                            <hr>


                            <div class="col-xs-6 pull-right">
                                <address style="width: 150px">
                                    <strong>Billed To:</strong><br>
                                    {{$i->inv_street}}, {{$i->inv_locality}}<br>
                                    {{$i->inv_city}}<br>
                                    {{$i->inv_city}}, {{$i->inv_state}} <br>
                                    {{$i->inv_country}}, {{$i->inv_pin}} <br>
                                </address>
                                <address style="width: 150px">
                                    <strong>Shipped To:</strong><br>
                                    {{$i->inv_customer_shipping_address}}
                                </address>
                            </div>
                            <div class="col-xs-6 ">
                                <address>
                                    Invoice No: <strong>{{$i->inv_id}}</strong> <br>
                                    Invoice Date: <strong>{{$i->inv_date_of_purchase}}</strong> <br>
                                    Invoice Reference: <strong>{{$i->inv_id}}</strong> <br>
                                    Due Date: <strong>{{$i->inv_due_date}}</strong> <br>
                                    Return Month:<strong>{{$i->inv_return_month}}</strong> <br>
                                    Return Quater: <strong>{{$i->inv_return_month}}</strong> <br>
                                </address>

                                <address>
                                    Customer Name: <strong>{{$i->inv_customer_name}}</strong> <br>
                                    GSTIN: <strong>{{$i->inv_customer_gstin}}</strong> <br>
                                    Place of Supply: <strong>{{$i->inv_customer_state}}</strong> <br>
                                </address>
                            </div>


                            <div class="col-xs-6 pull-right">

                            </div>
                            <div class="col-xs-6">

                            </div>

                        </div>


                        <div class="col-md-12">


                            <div class="m-section__content">
                                <table class="table m-table m-table--head-bg-success">
                                    <thead>
                                    <tr>
                                        <td><strong>S No</strong></td>
                                        <td class="text-center"><strong>Name</strong></td>
                                        <td class="text-center"><strong>Type</strong></td>
                                        <td class="text-center"><strong>HSN/SAC</strong></td>
                                        <td class="text-center"><strong>QTY</strong></td>
                                        <td class="text-center"><strong>Unit</strong></td>
                                        <td class="text-center"><strong>Rate</strong></td>
                                        <td class="text-center"><strong>CGST</strong></td>
                                        <td class="text-center"><strong>IGST</strong></td>
                                        <td class="text-center"><strong>SGST</strong></td>
                                        <td class="text-center"><strong>CESS</strong></td>
                                        <td class="text-center"><strong>TAX AMT</strong></td>
                                        <td class="text-center"><strong>Total</strong></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                    @foreach ($i->invoiceitems as $item)
                                        <tr>

                                            <td class="text-center">{{ $item->item_sno }}</td>
                                            <td class="text-center">{{ $item->item_name }}</td>
                                            <td class="text-center">{{ $item->item_type }}</td>
                                            <td class="text-center">{{ $item->item_hsn_sac }}</td>
                                            <td class="text-center">{{ $item->item_qty }}</td>
                                            <td class="text-center">{{ $item->item_unit }}</td>
                                            <td class="text-center">{{ $item->item_rate }}</td>
                                            <td class="text-center">{{ $item->item_tax_value }}</td>
                                            <td class="text-center">{{ $item->item_tax_cgst }}</td>
                                            <td class="text-center">{{ $item->item_tax_sgst }}</td>
                                            <td class="text-center">{{ $item->item_tax_igst }}</td>
                                            <td class="text-center">{{ $item->item_tax_value }}</td>
                                            <td class="text-center">{{ $item->item_total }}</td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>


                        </div>


                        <div class="m-section__content">
                            <div class="m-demo" data-code-preview="true" data-code-html="true" data-code-js="false">
                                <div class="">
                                    <div class="m-stack m-stack--ver m-stack--general m-stack--demo">


                                        <div class="m-stack__item" style="border: none">
                                            <h5 class="m-widget1__title" style="text-align: center">
                                                Total
                                            </h5>
                                            <span class="m-widget1__number m--font-primary">
												<h3 style="text-align: center"><i class="la la-rupee"></i>
                                                    <spam class="invoice_tax_value">
                                                                {{$i->inv_total_value}}
                                                    </spam>
                                                 </h3>
											</span>
                                        </div>
                                        <div class="m-stack__item" style="border: none">
                                            <h5 class="m-widget1__title" style="text-align: center">
                                                Total Tax Value
                                            </h5>
                                            <span class="m-widget1__number m--font-primary">
												<h3 style="text-align: center"><i class="la la-rupee"></i>
                                                    <spam class="invoice_tax_value">
                                                                {{$i->inv_total_tax_igst_value}}
                                                    </spam>
                                                 </h3>
											</span>
                                        </div>
                                        <div class="m-stack__item" style="border: none">
                                            <h5 class="m-widget1__title" style="text-align: center">
                                                Tot Tax.
                                            </h5>
                                            <span class="m-widget1__number m--font-primary">
												<h3 style="text-align: center"><i class="la la-rupee"></i>
                                                    <spam class="invoice_tax_value">
                                                                {{$i->inv_total_value - $i->inv_total_tax_igst_value }}
                                                    </spam>
                                                 </h3>
											</span>
                                        </div>

                                        <div class="m-stack__item" style="border: none">
                                            <h5 class="m-widget1__title" style="text-align: center">
                                                Discount
                                            </h5>
                                            <span class="m-widget1__number m--font-primary">
												<h3 style="text-align: center"><i class="la la-rupee"></i>
                                                    <spam class="invoice_tax_value">
                                                                @if(empty($i->inv_total_discount_value == 'NULL'))
                                                            0
                                                        @else
                                                            {{$i->inv_total_discount_value}}
                                                        @endif
                                                    </spam>
                                                 </h3>
											</span>
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>


                <div class="m-content">
                    <div class="m-section__content" style="text-align: center">
                        <div class="m-demo__preview m-demo__preview--btn">

                            <a href="{{route('invoice.pdf.download', $i->id)}}"
                               class="btn m-btn--pill m-btn--air btn-primary btn-lg m-btn--icon">
															<span>
																<i class="la la-archive"></i>
																<span>
																	Download Invoice
																</span>
															</span>
                            </a>
                            <button id="print" type="button"
                                    class="btn m-btn--pill m-btn--air m-btn--icon btn-secondary btn-lg print">
                                Print Invoice
                            </button>
                            <button type="button"
                                    class="btn m-btn--pill m-btn--air   m-btn--icon      btn-success btn-lg">
                                Mark As Paid
                            </button>

                            <button type="button"
                                    class="btn m-btn--pill m-btn--air   m-btn--icon      btn-danger btn-lg">
                                Delete Invoice
                            </button>
                        </div>
                    </div>
                </div>


                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>




@endsection

@section('client_footer')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jQuery.print/1.5.1/jQuery.print.min.js"></script>
    <script>
        $('#print').click(function (e) {
            e.preventDefault();
            $("#printable").print({
                globalStyles: true,
                mediaPrint: false,
                stylesheet: true,
                noPrintSelector: ".no-print",
                iframe: true,
                append: null,
                prepend: null,
                manuallyCopyFormValues: true,
                deferred: $.Deferred(),
                timeout: 750,
                title: null,
                doctype: '<!doctype html>'
            });
        });
    </script>

@endsection
