@extends('client.structure')
@section('client.title', 'Product Details' )
@section('client.menu.product', 'm-menu__item--active')
@section('client')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Dashboard
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Product
											</span>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Product Details
											</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Product Details
                            </h3>
                        </div>
                    </div>
                </div>
                <!--begin::Form-->
                <div class="m-portlet">
                    <div class="m-portlet__body m-portlet__body--no-padding">
                        <div class="row m-row--no-padding m-row--col-separator-xl">
                            <div class="col-md-12 col-lg-12 col-xl-4">
                                <!--begin:: Widgets/Stats2-1 -->
                                <div class="m-widget1">
                                    <div class="m-widget1__item">
                                        <div class="row m-row--no-padding align-items-center">
                                            <div class="col">
                                                {!! QrCode::size(100)->generate(Request::url()); !!}
                                            </div>
                                            <div class="col m--align-right">
														<span class="m-widget1__number m--font-brand">
															{{$p->name}}
														</span>
                                                <span class="m-widget1__desc">
															{{$p->description}}
														</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-widget1__item">
                                        <div class="row m-row--no-padding align-items-center">
                                            <div class="col">
                                                <h3 class="m-widget1__title">
                                                    Orders
                                                </h3>
                                                <span class="m-widget1__desc">
															Weekly Customer Orders
														</span>
                                            </div>
                                            <div class="col m--align-right">
														<span class="m-widget1__number m--font-danger">
															+1,800
														</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-widget1__item">
                                        <div class="row m-row--no-padding align-items-center">
                                            <div class="col">
                                                <h3 class="m-widget1__title">
                                                    Issue Reports
                                                </h3>
                                                <span class="m-widget1__desc">
															System bugs and issues
														</span>
                                            </div>
                                            <div class="col m--align-right">
														<span class="m-widget1__number m--font-success">
															-27,49%
														</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end:: Widgets/Stats2-1 -->
                            </div>
                            <div class="col-md-12 col-lg-12 col-xl-4">
                                <!--begin:: Widgets/Stats2-2 -->
                                <div class="m-widget1">
                                    <div class="m-widget1__item">
                                        <div class="row m-row--no-padding align-items-center">
                                            <div class="col">
                                                <h3 class="m-widget1__title">
                                                    IPO Margin
                                                </h3>
                                                <span class="m-widget1__desc">
															Awerage IPO Margin
														</span>
                                            </div>
                                            <div class="col m--align-right">
														<span class="m-widget1__number m--font-accent">
															+24%
														</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-widget1__item">
                                        <div class="row m-row--no-padding align-items-center">
                                            <div class="col">
                                                <h3 class="m-widget1__title">
                                                    Payments
                                                </h3>
                                                <span class="m-widget1__desc">
															Yearly Expenses
														</span>
                                            </div>
                                            <div class="col m--align-right">
														<span class="m-widget1__number m--font-info">
															+$560,800
														</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-widget1__item">
                                        <div class="row m-row--no-padding align-items-center">
                                            <div class="col">
                                                <h3 class="m-widget1__title">
                                                    Logistics
                                                </h3>
                                                <span class="m-widget1__desc">
															Overall Regional Logistics
														</span>
                                            </div>
                                            <div class="col m--align-right">
														<span class="m-widget1__number m--font-warning">
															-10%
														</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--begin:: Widgets/Stats2-2 -->
                            </div>
                            <div class="col-md-12 col-lg-12 col-xl-4">
                                <!--begin:: Widgets/Stats2-3 -->
                                <div class="m-widget1">
                                    <div class="m-widget1__item">
                                        <div class="row m-row--no-padding align-items-center">
                                            <div class="col">
                                                <h3 class="m-widget1__title">
                                                    Orders
                                                </h3>
                                                <span class="m-widget1__desc">
															Awerage Weekly Orders
														</span>
                                            </div>
                                            <div class="col m--align-right">
														<span class="m-widget1__number m--font-success">
															+15%
														</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-widget1__item">
                                        <div class="row m-row--no-padding align-items-center">
                                            <div class="col">
                                                <h3 class="m-widget1__title">
                                                    Transactions
                                                </h3>
                                                <span class="m-widget1__desc">
															Daily Transaction Increase
														</span>
                                            </div>
                                            <div class="col m--align-right">
														<span class="m-widget1__number m--font-danger">
															+80%
														</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="m-widget1__item">
                                        <div class="row m-row--no-padding align-items-center">
                                            <div class="col">
                                                <h3 class="m-widget1__title">
                                                    Revenue
                                                </h3>
                                                <span class="m-widget1__desc">
															Overall Revenue Increase
														</span>
                                            </div>
                                            <div class="col m--align-right">
														<span class="m-widget1__number m--font-primary">
															+60%
														</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--begin:: Widgets/Stats2-3 -->
                            </div>
                        </div>
                    </div>
                </div>
                <!--End::Main Portlet-->

                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
    @endsection