@extends('client.structure')
@section('client.title', 'Create New Product')
@section('client.menu.product', 'm-menu__item--active')
@section('client')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Dashboard
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Product
											</span>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Create New Product
											</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Add new Product
                            </h3>
                        </div>
                    </div>


                </div>

                <!--begin::Form to add product-->
                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed" method="POST"
                      action="{{route('product.store')}}">
                    {{ csrf_field() }}

                    <div class="m-portlet__body">

                        @if ($errors->any())

                            <div class="form-group m-form__group row">
                                <div class="col-lg-12">


                                    <div class="form-group m-form__group row">

                                        <div class="col-lg-12 col-md-12 col-sm-12">

                                            <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger alert-dismissible fade show"
                                                 role="alert">
                                                <div class="m-alert__icon">
                                                    <i class="flaticon-exclamation-1"></i>
                                                    <span></span>
                                                </div>
                                                <div class="m-alert__text">
                                                    <strong>
                                                        OOPS! <br>
                                                    </strong>
                                                    @foreach ($errors->all() as $error)
                                                        {{ $error }} <br>

                                                    @endforeach
                                                </div>
                                                <div class="m-alert__close">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                            aria-label="Close"></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>

                        @endif

                        <div class="form-group m-form__group row">
                            <div class="col-lg-4">
                                <label>
                                    Product Name:
                                </label>
                                <input type="text" name="name" class="form-control m-input--air" placeholder="Required">
                                <span class="m-form__help">
														Name full name of the Product
													</span>
                            </div>
                            <div class="col-lg-4">
                                <label class="">
                                    Short Description:
                                </label>
                                <input type="text" name="description" class="form-control m-input--air"
                                       placeholder="Optional">
                                <span class="m-form__help">
														Short Description About Product
													</span>
                            </div>

                            <div class="col-lg-4">
                                <label>
                                    HSN CODE:
                                </label>
                                <div class="input-group m-input-group m-input-group--square">

                                    <input name="hsn" id="hsnfetch" type="text"
                                           class="form-control m-input--air ui-widget" placeholder="Required">
                                </div>
                                <span class="m-form__help">
														Enter HSN of the Product
													</span>
                            </div>


                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-3">
                                <label class="">
                                    Type:
                                </label>
                                <div class="input-group m-input-group m-input-group--square">

                                    <select name="item_type" class="form-control m-input--air">

                                        <option>
                                            Goods
                                        </option>
                                        <option>
                                           Service
                                        </option>
                                    </select>

                                </div>
                                <span class="m-form__help">
														Select Service/ Product
													</span>
                            </div>
                            <div class="col-lg-3">
                                <label class="">
                                    IGST:
                                </label>
                                <div class="input-group m-input-group m-input-group--square">

                                    <select name="igst" class="form-control m-input--air igst igstselect">
                                        <option>
                                            0
                                        </option>
                                        <option>
                                            0.25
                                        </option>
                                        <option>
                                            3
                                        </option>
                                        <option>
                                            5
                                        </option>
                                        <option>
                                            12
                                        </option>
                                        <option>
                                            18
                                        </option>
                                        <option>
                                            28
                                        </option>
                                    </select>
                                    <span class="input-group-addon">
															%
														</span>
                                </div>
                                <span class="m-form__help">
                                    IGST is auto calculated
													</span>
                            </div>
                            <div class="col-lg-3">
                                <label class="">
                                    SGST:
                                </label>
                                <div class="input-group m-input-group m-input-group--square">

                                    <select name="sgst" readonly class="form-control m-input--air sgst">
                                        <option>
                                            0
                                        </option>
                                        <option>
                                            0.125
                                        </option>
                                        <option>
                                            1.5
                                        </option>
                                        <option>
                                            2.5
                                        </option>
                                        <option>
                                            6
                                        </option>
                                        <option>
                                            9
                                        </option>
                                        <option>
                                            14
                                        </option>
                                    </select>
                                    <span class="input-group-addon">
															%
														</span>
                                </div>
                                <span class="m-form__help">
														Enter SGST of the product
													</span>
                            </div>
                            <div class="col-lg-3">
                                <label class="">
                                    CGST:
                                </label>
                                <div class="input-group m-input-group m-input-group--square">

                                    <select name="cgst" readonly class="form-control m-input--air cgst">
                                        <option>
                                            0
                                        </option>
                                        <option>
                                            0.125
                                        </option>
                                        <option>
                                            1.5
                                        </option>
                                        <option>
                                            2.5
                                        </option>
                                        <option>
                                            6
                                        </option>
                                        <option>
                                            9
                                        </option>
                                        <option>
                                            14
                                        </option>
                                    </select>
                                    <span class="input-group-addon">
															%
														</span>
                                </div>
                                <span class="m-form__help">
														Enter CGST of the product
													</span>
                            </div>

                        </div>
                        <div class="form-group m-form__group row">

                            <div class="col-lg-3">
                                <label>
                                    Price:
                                </label>
                                <div class="input-group m-input-group m-input-group--square">
														<span class="input-group-addon">
															<i class="la la-inr"></i>
														</span>
                                    <input name="opeaning_rate" type="number"
                                           class="form-control m-input--air pricewithtax priceupdate" placeholder="">
                                </div>
                                <span class="m-form__help">
														Opening Price of the Product
													</span>
                            </div>


                            <div class="col-lg-3">
                                <label>
                                    Price (without Tax):
                                </label>
                                <div class="input-group m-input-group m-input-group--square">
														<span class="input-group-addon">
															<i class="la la-inr"></i>
														</span>
                                    <input name="opeaning_taxable_rate" type="text"
                                           class="form-control m-input--air pricewttaxupdate pricewttax"
                                           placeholder="">
                                </div>
                                <span class="m-form__help">
                                    Price of the Product (without Tax)
													</span>
                            </div>


                            <div class="col-lg-3">
                                <label>
                                    Unit Definition:
                                </label>
                                <div class="m-input-icon m-input-icon--right">
                                    <select class="form-control m-input--air"
                                            name="unit_definition">
                                        <option value="cm">CM</option>
                                        <option value="MM">MM</option>
                                        <option value="KG">KG</option>
                                        <option value="PIECE">PIECE</option>
                                        <option value="GM">GM</option>
                                        <option value="UNIT">UNITS</option>
                                        <option value="OTHER">OTHER</option>

                                    </select>

                                </div>
                                <span class="m-form__help">
														Unit Definition
													</span>
                            </div>


                            <div class="col-lg-3">
                                <label>
                                    Conversion Rate:
                                </label>
                                <div class="input-group m-input-group m-input-group--square">

                                    <input type="text" name="conversion_rate" class="form-control m-input--air"
                                           placeholder="Required">
                                </div>
                                <span class="m-form__help">
														Conversion Rate of the Product
													</span>
                            </div>


                        </div>
                    </div>


                    <div class="m-form m-form--fit m-form--label-align-right">
                        <div class="form-group m-form__group row">
                            <div class="row">
                                <div class="col-lg-6">
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                                <div class="col-lg-6">

                                    <button type="reset" class="btn btn-secondary">
                                        Reset
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>


                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>

    <style>
        input.ui-autocomplete-loading {
            background: white url("../../../loader/ajaxloader.gif") right center no-repeat;
            background-size: 32px 32px;
        }
    </style>


@endsection

@section('client_footer')
    <script src="{{ asset('js/seclect2.js')}}" type="text/javascript"></script>
    <link rel="stylesheet" href="{{ asset('css/client/jquery-ui.min.css')}}"/>
    <link rel="stylesheet" href="{{ asset('css/client/jquery-ui.theme.min.css')}}"/>
    <script src="{{ asset('js/client/jquery-ui.min.js')}}" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            var src = "{{ route('api.hsn.fetch.search') }}";
            $("#hsnfetch").autocomplete({
                maxShowItems: 2,
                source: function (request, response) {
                    $.ajax({
                        url: src,
                        dataType: "json",
                        data: {
                            term: request.term
                        },
                        success: function (data) {
                            response(data);

                        }


                    });
                },
                minLength: 3,
                select: function (event, ui) {
                    event.preventDefault();
                    $(this).val(ui.item.value);
                    $('.igst').val(ui.item.tax);
                    $('.cgst').val(ui.item.tax / 2);
                    $('.sgst').val(ui.item.tax / 2);


                }

            })
                .data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li style='width: 300px;'>")
                    .append("<a>" + item.label +
                        "<br><span style='font-size: 100%; '>GST:" + item.tax + "%</span>" +
                        "<br><span style='font-size: 70%;'>" + item.description + "</span>"
                    )
                    .appendTo(ul);
            };


            $('.priceupdate').on('keyup', function () {
                var pricewtax = $(".pricewithtax").val();
                var pricewttax = $(".pricewttax").val();
                var taxper = $(".igst").val();

                // var taxrate = $(".taxper").find(":selected").text();

                // Update individual price
                var total = 0;
                var finalpricewtax = pricewttax - taxper / 100;
                var taxp = taxper / 100 + 1;
                var finalpricewttax = pricewtax / taxp;
                // $('.pricewithtax').val(finalpricewtax );
                $('.pricewttax').val(finalpricewttax.toFixed(2));


            })

                .trigger('keyup');

            $('.pricewttaxupdate').on('keyup', function () {

                var pricewttax = $(".pricewttax").val();
                var taxper = $(".igst").val();


                var taxp = taxper / 100 + 1;
                var finalpricewithtax = pricewttax * taxp;
                // $('.pricewithtax').val(finalpricewtax );
                $('.pricewithtax').val(finalpricewithtax.toFixed(2));


            })
                .trigger('keyup');


            $('.igstselect').change(function () {

                var igstselect = $(".igstselect").val();
                var cgstigstcalcu = igstselect / 2;
                console.log(cgstigstcalcu);
                $('.cgst').val(cgstigstcalcu);
                $('.sgst').val(cgstigstcalcu);


            });


        });
    </script>
@endsection