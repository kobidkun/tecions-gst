@extends('client.structure')
@section('client.title', 'Edit Product ')
@section('client.menu.product', 'm-menu__item--active')
@section('client')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Dashboard
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="#" class="m-nav__link">
											<span class="m-nav__link-text">
												Product
											</span>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Edit Product
											</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->
        <div class="m-content">
            <!--begin::Portlet-->
            <div class="m-portlet">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Edit Product
                            </h3>
                        </div>
                    </div>


                </div>

                <!--begin::Form to add product-->
                <form class="m-form m-form--fit m-form--label-align-right m-form--group-seperator-dashed"
                      method="POST"  action="{{route('product.update', ['id' => $f->id])}}">
                    {{ csrf_field() }}

                    {{ method_field('PATCH') }}

                    <div class="m-portletbody">

                        @if ($errors->any())

                            <div class="form-group m-form__group row">
                                <div class="col-lg-12">



                                    <div class="form-group m-form__group row">

                                        <div class="col-lg-12 col-md-12 col-sm-12">

                                            <div class="m-alert m-alert--icon m-alert--icon-solid m-alert--outline alert alert-danger alert-dismissible fade show" role="alert">
                                                <div class="m-alert__icon">
                                                    <i class="flaticon-exclamation-1"></i>
                                                    <span></span>
                                                </div>
                                                <div class="m-alert__text">
                                                    <strong>
                                                        OOPS! <br>
                                                    </strong>
                                                    @foreach ($errors->all() as $error)
                                                        {{ $error }} <br>

                                                    @endforeach
                                                </div>
                                                <div class="m-alert__close">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>

                        @endif

                        <div class="form-group m-form__group row">
                            <div class="col-lg-4">
                                <label>
                                    Product Name:
                                </label>
                                <input type="text" name="name"
                                       value="{{$f->name}}"
                                       class="form-control m-input--air" placeholder="Required">
                                <span class="m-form__help">
														Name full name of the Product
													</span>
                            </div>
                            <div class="col-lg-4">
                                <label class="">
                                    Short Description:
                                </label>
                                <input type="text" value="{{$f->description}}" name="description" class="form-control m-input--air" placeholder="Optional">
                                <span class="m-form__help">
														Short Description About Product
													</span>
                            </div>
                            <div class="col-lg-4">
                                <label>
                                    Price:
                                </label>
                                <div class="input-group m-input-group m-input-group--square">
														<span class="input-group-addon">
															<i class="la la-inr"></i>
														</span>
                                    <input name="opeaning_rate" value="{{$f->opeaning_rate}}" type="number" class="form-control m-input--air" placeholder="">
                                </div>
                                <span class="m-form__help">
														Opening Price of the Product
													</span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-4">
                                <label class="">
                                    SGST:
                                </label>
                                <select name="sgst" class="form-control m-input--air">

                                    <option value="{{$f->sgst}}">{{$f->sgst}}%</option>
                                    <option value="0">0%</option>
                                    <option value="2.5">2.5%</option>
                                    <option value="4.5">4.5%</option>
                                    <option value="6">6%</option>
                                    <option value="9">9%</option>
                                    <option value="14">14%</option>
                                </select>
                                <span class="m-form__help">
														Enter SGST of the product
													</span>
                            </div>
                            <div class="col-lg-4">
                                <label class="">
                                    CGST:
                                </label>
                                <select name="cgst" class="form-control m-input--air">
                                    <option value="{{$f->cgst}}">{{$f->cgst}}%</option>
                                    <option value="0">0%</option>
                                    <option value="2.5">2.5%</option>
                                    <option value="4.5">4.5%</option>
                                    <option value="6">6%</option>
                                    <option value="9">9%</option>
                                    <option value="14">14%</option>
                                </select>
                                <span class="m-form__help">
														Enter CGST of the product
													</span>
                            </div>
                            <div class="col-lg-4">
                                <label class="">
                                    IGST:
                                </label>
                                <select name="igst" class="form-control m-input--air">
                                    <option value="{{$f->igst}}">{{$f->igst}}%</option>
                                    <option value="2.5">2.5%</option>
                                    <option value="4.5">4.5%</option>
                                    <option value="6">6%</option>
                                    <option value="9">9%</option>
                                    <option value="14">14%</option>
                                </select>
                                <span class="m-form__help">
                                    IGST is auto calculated
													</span>
                            </div>
                        </div>
                        <div class="form-group m-form__group row">
                            <div class="col-lg-4">
                                <label >
                                    Unit Definition:
                                </label>
                                <div class="m-input-icon m-input-icon--right">
                                    <select class="form-control m-input--air"
                                            name="unit_definition">
                                        <option value="{{$f->unit_definition}}">{{$f->unit_definition}}</option>
                                        <option value="cm">CM</option>
                                        <option value="MM">MM</option>
                                        <option value="KG">KG</option>
                                        <option value="PIECE">PIECE</option>
                                        <option value="GM">GM</option>
                                        <option value="UNIT">UNITS</option>
                                        <option value="OTHER">OTHER</option>

                                    </select>

                                </div>
                                <span class="m-form__help">
														Unit Definition
													</span>
                            </div>
                            <div class="col-lg-4">
                                <label>
                                    HSN CODE:
                                </label>
                                <div class="input-group m-input-group m-input-group--square">

                                    <input name="hsn" type="text" value="{{$f->hsn}}" class="form-control m-input--air" placeholder="Required">
                                </div>
                                <span class="m-form__help">
														Enter HSN of the Product
													</span>
                            </div>


                            <div class="col-lg-4">
                                <label>
                                    Conversion Rate:
                                </label>
                                <div class="input-group m-input-group m-input-group--square">

                                    <input type="text" name="conversion_rate" value="{{$f->conversion_rate}}" class="form-control m-input--air" placeholder="Required">
                                </div>
                                <span class="m-form__help">
														Conversion Rate of the Product
													</span>
                            </div>




                        </div>
                    </div>




                    <div class="m-form m-form--fit m-form--label-align-right">
                        <div class="form-group m-form__group row">
                            <div class="row">
                                <div class="col-lg-6">
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                                <div class="col-lg-6">

                                    <a href="/product" class="btn btn-secondary">
Back
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>



                </form>
                <!--end::Form-->
            </div>
            <!--end::Portlet-->
        </div>
    </div>
@endsection

@section('client_footer')
    <script src="{{ asset('js/seclect2.js')}}" type="text/javascript"></script>
@endsection