@extends('client.structure')
@section('client.title', 'All Products')
@section('client.menu.product', 'm-menu__item--active')
@section('client')
    <link rel="stylesheet" href="{{asset('css/client/datatable.min.css')}}">


    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <!-- BEGIN: Subheader -->
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title m-subheader__title--separator">
                        Dashboard
                    </h3>
                    <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                        <li class="m-nav__item m-nav__item--home">
                            <a href="#" class="m-nav__link m-nav__link--icon">
                                <i class="m-nav__link-icon la la-home"></i>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												Product
											</span>
                            </a>
                        </li>
                        <li class="m-nav__separator">
                            -
                        </li>
                        <li class="m-nav__item">
                            <a href="" class="m-nav__link">
											<span class="m-nav__link-text">
												All Product
											</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div>
                </div>
            </div>
        </div>
        <!-- END: Subheader -->


        <div class="m-content">
            <div class="row">
                <div class="col-xl-12">
                    <!--begin::Portlet-->
                    <div class="m-portlet">
                        <div class="m-portlet__head">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text">View All Products

                                    </h3>

                                </div>
                            </div>

                            <div class="m-portlet__head-tools">
                                <ul class="m-portlet__nav">
                                    <li class="m-portlet__nav-item">
                                        <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push"
                                             data-dropdown-toggle="hover" aria-expanded="true">
                                            <a href="{{route('product.create')}}"
                                               class="btn btn-outline-primary m-btn m-btn--icon m-btn--outline-2x m-btn--pill m-btn--air">
															<span>
																<i class="la la-industry"></i>
																<span>
																	Add New Product
																</span>
															</span>
                                            </a>

                                        </div>
                                    </li>
                                </ul>
                            </div>


                        </div>


                        <div class="m-portlet__body">
                            <!--begin::Section-->
                            <div class="m-section">


                                <div class="m-section__content">
                                    <table class="table table-bordered product-table" id="product-table">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Type</th>
                                            <th>GSTIN</th>
                                            <th>AMOUNT</th>
                                            <th>CREATED At</th>
                                            <th>Details</th>
                                        </tr>
                                        </thead>
                                    </table>


                                </div>


                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection

@section('client_footer')
    <script src="{{asset('js/client/datatable.min.js')}}"></script>
    <script>
        $(function () {
            $('#product-table').DataTable({
                processing: true,
                serverSide: true,
                oLanguage: {sProcessing: '<div class="" style="width: 500px; height:100%; display: inline-block;z-index: 700"><img src="{{asset('/loader/ajaxloader.gif')}}" alt=""></div>'},

                ajax: '{{route('api.get.product')}}',
                columns: [
                    {data: 'name', name: 'name'},
                    {data: 'item_type', name: 'item_type'},
                    {data: 'igst', name: 'igst'},
                    {data: 'hsn', name: 'hsn'},

                    {data: 'opeaning_taxable_rate', name: 'action', orderable: false, searchable: false},
                    {data: 'action', name: 'action', orderable: false, searchable: false}


                ]
            });

        });
    </script>

@endsection

