let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.scripts([
    'resources/assets/client/js/vendors.bundle.js',
    'resources/assets/client/js/scripts.bundle.js'

], 'public/js/clientbase.min.js');


mix.styles([
    'resources/assets/client/css/style.bundle.css',
    'resources/assets/client/css/vendors.bundle.css'
], 'public/css/clientbase.min.css');
