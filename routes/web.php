<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



//texting purpose
Route::post('/', 'testcontroller@save')->name('test.post');
Route::get('/', 'testcontroller@index');

//end testing purpose

//manageinvoice

Route::post('/invoice', 'invoice\ManageInvoice@store')->name('inv.store.post');
Route::get('/invoice/create', 'invoice\ManageInvoice@create')->name('invoice.create.get');
Route::get('/invoice/{id}/download/pdf', 'invoice\ManageInvoice@generatePdfInvoice')->name('invoice.pdf.download');
Route::get('/invoice/{id}/details', 'invoice\ManageInvoice@InvoiceDetails')->name('invoice.details');
Route::get('/invoice', 'invoice\ManageInvoice@index')->name('invoice.dashboard.get');


//product Manage
Route::resource('product', 'product\ManageProduct');
Route::get('product/view/{id}', 'product\ManageProduct@viewdetails');
//end product Manage
//
///
///  //product client
Route::resource('customer', 'customer\CustomerController');

//end product client


