<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('product', 'product\ManageProduct@productapi')->name('api.get.product');
Route::get('invoice/api-getinvoice', 'invoice\ManageInvoice@invoicetapi')->name('api.get.invoice');


Route::get('customer', 'customer\CustomerController@customerapi')->name('api.get.customer');
Route::get('customer/apisearch', 'customer\CustomerController@apisearch')->name('api.get.customer.apisearch');


Route::get('product/api-fetch', 'product\ManageProduct@apisearch')->name('api.product.fetch.search');


Route::get('hsn/api-fetch', 'HsnController@index')->name('api.hsn.fetch.search');
