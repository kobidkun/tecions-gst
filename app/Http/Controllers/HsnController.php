<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HsnList;
class HsnController extends Controller
{
    public function index(Request $request) {
        $query = $request->get('term','');
//or
        $products=HsnList::where('code','LIKE','%'.$query.'%')->orWhere('description','LIKE','%'.$query.'%')->get();

        $data=array();
        foreach ($products as $product) {
            $data[]=array('value'=>$product->code,
                'id'=>$product->id,
                'description'=>$product->description,
                'tax'=>$product->tax

            );
        }
        if(count($data))
            return $data;
        else
            return ['value'=>'No Result Found','description'=>'not value'];
    }
}
