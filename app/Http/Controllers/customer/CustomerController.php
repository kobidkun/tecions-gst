<?php

namespace App\Http\Controllers\customer;

use App\Customer;
use App\Http\Controllers\Controller;
use App\Http\Requests\customer as customerrequest;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('client.pages.customer.showall');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.pages.customer.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(customerrequest $request)
    {
        $s = new Customer();
        $s->name = $request->name;
        $s->mobile = $request->mobile;
        $s->email = $request->email;
        $s->person_name = $request->person_name;
        $s->street = $request->street;
        $s->locality = $request->locality;
        $s->city = $request->city;
        $s->state = $request->state;
        $s->country = $request->country;
        $s->pin = $request->pin;
        $s->gstin = $request->gstin;
        $s->save();
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function customerapi() {
        $g = Customer::all();
        $c = $g->count();
        return Datatables::of(Customer::query())->make(true);
    }

    public function apisearch(Request $request) {
        $query = $request->get('term','');

        $products = Customer::where('name', 'LIKE', '%' . $query . '%')->take(5)->get();

        $data=array();
        foreach ($products as $product) {
            $data[]=array('value'=>$product->name,
                'id'=>$product->id,
                'gstin'=>$product->gstin,
                'street'=>$product->street,
                'locality'=>$product->locality,
                'city'=>$product->city,
                'state'=>$product->state,
                'country'=>$product->country,
                'pin' => $product->pin,
                'person_name' => $product->person_name

            );
        }
        if(count($data))
            return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }

}
