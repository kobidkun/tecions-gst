<?php

namespace App\Http\Controllers\product;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductAdd;
use App\Product;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class ManageProduct extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('client.pages.product.showallproduct');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('client.pages.product.createproduct');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductAdd $request)
    {
        $s = new Product();
        $s->name = $request->name;
        $s->item_type = $request->item_type;
        $s->catogory_id = $request->catogory_id;
        $s->unit_definition = $request->unit_definition;
        $s->description = $request->description;
        $s->gst_apply = $request->gst_apply;
        $s->service_type = $request->service_type;
        $s->igst = $request->igst;
        $s->cgst = $request->cgst;
        $s->sgst = $request->sgst;
        $s->hsn = $request->hsn;
        $s->conversion_rate = $request->conversion_rate;
        $s->opeaning_quantity = $request->opeaning_quantity;
        $s->opeaning_rate = $request->opeaning_rate;
        $s->opeaning_value = $request->opeaning_value;
        $s->opeaning_taxable_rate = $request->opeaning_taxable_rate;
        $s->save();
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $p = Product::find($id);
        return view('client.pages.product.productdetails')->with(['p' => $p]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $f = Product::find($id);
        // dd($f);
        return view('client.pages.product.editproduct', ['f' => $f]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $s = Product::find($id);
        $s->name = $request->name;
        $s->item_type = $request->item_type;
        $s->catogory_id = $request->catogory_id;
        $s->unit_definition = $request->unit_definition;
        $s->description = $request->description;
        $s->gst_apply = $request->gst_apply;
        $s->service_type = $request->service_type;
        $s->igst = $request->igst;
        $s->cgst = $request->cgst;
        $s->sgst = $request->sgst;
        $s->hsn = $request->hsn;
        $s->conversion_rate = $request->conversion_rate;
        $s->opeaning_quantity = $request->opeaning_quantity;
        $s->opeaning_rate = $request->opeaning_rate;
        $s->opeaning_value = $request->opeaning_value;
        $s->save();
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);
        //  $f->delete();
        return back();
    }

    public function productapi() {
        $i = Product::select(
            [
                'id',
                'name',
                'item_type',
                'igst',
                'hsn',
                'opeaning_taxable_rate'
            ]);

        return Datatables::of($i)
            ->addColumn('action', function ($i) {
                // return '<a href="/invoice/'.$i->id.'/details" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> View Details</a>';
                return '
                <div class="btn-group dropup"><a href="/product/' . $i->id . '/edit">
																<button type="button" class="btn btn-outline-success">
																	Edit Product
																</button>
</a>
																<button type="button" class="btn btn-outline-success dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	<span class="sr-only">
																		Toggle Dropdown
																	</span>
																</button>
																<div class="dropdown-menu">
																	
																	<div class="dropdown-divider"></div>
																	<a class="dropdown-item" href="#">
																		Delete Invoice
																	</a>
																</div>
															</div>';
            })
            ->editColumn('id', 'ID: {{$id}}')
            ->make(true);
    }


    public function apisearch(Request $request) {
        $query = $request->get('term','');

        $products = Product::where('name', 'LIKE', '%' . $query . '%')->take(5)->get();

        $data=array();
        foreach ($products as $product) {
            $data[]=array('value'=>$product->name,
                'id'=>$product->id,
                'service_type' => $product->item_type,
                'igst'=>$product->igst,
                'cgst'=>$product->cgst,
                'sgst'=>$product->sgst,
                'hsn'=>$product->hsn,
                'conversion_rate'=>$product->conversion_rate,
                'opeaning_rate'=>$product->opeaning_rate,
                'opeaning_taxable_rate' => $product->opeaning_taxable_rate,
                'unit_definition'=>$product->unit_definition

            );
        }
        if(count($data))
            return $data;
        else
            return ['value'=>'No Result Found','id'=>''];


    }

    public function viewdetails($id)
    {
        $p = Product::find($id);
        return view('client.pages.product.productdetails')->with(['p' => $p]);
    }
}
