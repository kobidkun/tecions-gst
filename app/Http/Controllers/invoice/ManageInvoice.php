<?php

namespace App\Http\Controllers\invoice;

use App\Http\Controllers\Controller;
use App\Invoice;
use Carbon\Carbon;
use Illuminate\Http\Request;
use PDF;
use Yajra\Datatables\Datatables;

class ManageInvoice extends Controller
{
    public function index()
    {


        return view('client.pages.invoice.showallinvoice');
    }

    public function create()
    {
        $lastInvoiceID = Invoice::orderBy('created_at', 'desc')->first();
        return view('client.pages.invoice.createinvoicebkp', ['invid' => $lastInvoiceID]);

    }

    public function store(Request $request)
    {


        $s = new Invoice();
        $s->inv_id = $request->inv_id;
        $s->inv_date_of_purchase = $request->inv_date_of_purchase;
        $s->inv_return_month = $request->inv_return_month;
        $s->inv_due_date = $request->inv_due_date;
        $s->inv_customer_name = $request->inv_customer_name;
        $s->inv_customer_gstin = $request->inv_customer_gstin;
        $s->inv_customer_state = $request->inv_customer_state;
        $s->inv_person_name = $request->inv_person_name;
        $s->inv_street = $request->inv_street;
        $s->inv_locality = $request->inv_locality;
        $s->inv_city = $request->inv_city;
        $s->inv_state = $request->inv_state;
        $s->inv_country = $request->inv_country;
        $s->inv_pin = $request->inv_pin;


        $s->inv_customer_shipping_address = $request->inv_customer_bill_address;
        $s->inv_total_tax_cgst_value = $request->inv_total_tax_cgst;
        $s->inv_total_tax_igst_value = $request->inv_total_tax_igst;
        $s->inv_total_tax_sgst_value = $request->inv_total_tax_cgst;
        $s->inv_total_tax_value = $request->inv_total_tax_value;
        $s->inv_total_value = $request->inv_total;
        $s->save();

        $invoice_id = $s->id;

        $data = $request->input('items');


        foreach ($data as $key => $value) {
            $s->invoiceitems()->create([
                'invoice_id' => $invoice_id,
                'item_sno' => $value['item_sno'],
                'item_name' => $value['item_name'],
                'item_type' => $value['item_type'],
                'item_hsn_sac' => $value['item_hsn_sac'],
                'item_qty' => $value['item_qty'],
                'item_unit' => $value['item_unit'],
                'item_total_taxable_rate' => $value['taxable_rate'],
                'item_rate' => $value['item_rate'],
                'unit_item_tax_value' => $value['unit_item_tax_value'],
                'item_tax_value' => $value['item_tax_value'],
                'item_tax_rate_percentage' => $value['item_tax_rate_percentage'],
                'item_tax_cgst' => $value['item_tax_cgst'],
                'item_tax_cgst_percentage' => $value['item_tax_cgst_percentage'],
                'item_tax_sgst' => $value['item_tax_sgst'],
                'item_tax_sgst_percentage' => $value['item_tax_sgst_percentage'],
                'item_tax_igst' => $value['item_tax_igst'],
                'item_tax_igst_percentage' => $value['item_tax_igst_percentage'],
                'item_tax_cess' => $value['item_tax_cess'],
                'item_tax_cess_percentage' => $value['item_tax_cess_percentage'],
                'item_total' => $value['item_total'],


            ]);
        }

        return response()->json(
            ['data' => 'success', 'id' => $invoice_id]
        );


    }


    public function InvoiceDetails($id)
    {

        $i = Invoice::find($id);
        return view('client.pages.invoice.invoicedetails', ['i' => $i]);

    }

    public function generatePdfInvoice($id)
    {
        $p = Invoice::find($id);
        $c = Carbon::now();
        $n = $p->inv_customer_name;
        $pdf = PDF::loadView('client.pdf.invoice', ['i' => $p]);
        return $pdf->download($n . '-' . $c . '-' . 'invoice.pdf');

    }

    public function invoicetapi()
    {


        $i = Invoice::select(
            [
                'id',
                'inv_id',
                'inv_customer_name',
                'inv_customer_gstin',
                'inv_total_value',
                'updated_at'
            ]);

        return Datatables::of($i)
            ->addColumn('action', function ($i) {
                // return '<a href="/invoice/'.$i->id.'/details" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> View Details</a>';
                return '
                <div class="btn-group dropup">
																<a href="/invoice/' . $i->id . '/details">
																<button type="button" class="btn btn-outline-success">
																	View Invoice
																</button>
</a>
																<button type="button" class="btn btn-outline-success dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																	<span class="sr-only">
																		Toggle Dropdown
																	</span>
																</button>
																<div class="dropdown-menu">
																	<a class="dropdown-item" href="/invoice/' . $i->id . '/download/pdf">
																		Download Invoice
																	</a>
																	<div class="dropdown-divider"></div>
																	<a class="dropdown-item" href="#">
																		Delete Invoice
																	</a>
																</div>
															</div>
                
                ';
            })
            ->editColumn('id', 'ID: {{$id}}')
            ->removeColumn('password')
            ->make(true);
    }

}
