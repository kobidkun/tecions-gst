<?php

namespace App\Http\Controllers;

use App\Invoice;
use Illuminate\Http\Request;

class ManageInvoice extends Controller
{
    public function create()
    {
        return view('client.pages.invoice.createinvoice');

    }

    public function store(Request $request)
    {


        $s = new Invoice();
        $s->inv_id = $request->inv_id;
        $s->inv_date_of_purchase = $request->inv_date_of_purchase;
        $s->inv_return_month = $request->inv_return_month;
        $s->inv_due_date = $request->inv_due_date;
        $s->inv_customer_name = $request->inv_customer_name;
        $s->inv_customer_gstin = $request->inv_customer_gstin;
        $s->inv_customer_state = $request->inv_customer_state;
        $s->inv_customer_bill_address = $request->inv_customer_bill_address;
        $s->inv_total_tax_cgst = $request->inv_total_tax_cgst;
        $s->inv_total_tax_igst = $request->inv_total_tax_igst;
        $s->inv_total_tax_sgst = $request->inv_total_tax_sgst;
        $s->inv_total = $request->inv_total;
        $s->save();

        $invoice_id = $s->id;

        $data = $request->input('items');


        foreach ($data as $key => $value) {
            $s->invoiceitems()->create([
                'invoice_id' => $invoice_id,
                'item_sno' => $value['item_sno'],
                'item_name' => $value['item_name'],
                'item_type' => $value['item_type'],
                'item_hsn_sac' => $value['item_hsn_sac'],
                'item_qty' => $value['item_qty'],
                'item_unit' => $value['item_unit'],
                'item_rate' => $value['item_rate'],
                'item_tax_value' => $value['item_name'],
                'item_tax_rate_percentage' => $value['item_tax_rate_percentage'],
                'item_tax_cgst' => $value['item_tax_cgst_percentage'],
                'item_tax_cgst_percentage' => $value['item_tax_cgst_percentage'],
                'item_tax_sgst' => $value['item_tax_sgst_percentage'],
                'item_tax_sgst_percentage' => $value['item_tax_sgst_percentage'],
                'item_tax_igst' => $value['item_tax_igst_percentage'],
                'item_tax_igst_percentage' => $value['item_tax_igst_percentage'],
                'item_tax_cess' => $value['item_tax_cess'],
                'item_tax_cess_percentage' => $value['item_tax_cess_percentage'],
                'item_total' => $value['item_total'],


            ]);
        }

        return response()->json(['data' => 'success']);


    }


    public function InvoiceDetails($id)
    {

        $i = Invoice::find($id);


        return view('client.pages.invoice.invoicedetails')->with(['d' => $i]);

    }

}
