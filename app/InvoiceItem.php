<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceItem extends Model
{
    protected $fillable = [
        'invoice_id',
        'item_sno',
        'item_name',
        'item_type',
        'item_hsn_sac',
        'item_qty',
        'item_unit',
        'unit_item_tax_value',
        'item_rate',
        'item_tax_value',
        'item_tax_rate_percentage',
        'item_tax_cgst',
        'item_tax_cgst_percentage',
        'item_tax_sgst',
        'item_tax_sgst_percentage',
        'item_tax_igst',
        'item_tax_igst_percentage',
        'item_tax_cess',
        'item_tax_cess_percentage',
        'item_total',
    ];

    public function invoice()
    {
        return $this->belongsTo('App\Invoice', 'id', 'invoice_id');
    }
}
