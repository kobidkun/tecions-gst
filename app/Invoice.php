<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{

    //protected $table = "Invoices";

    protected $fillable = [
        'id'
    ];

    public function invoiceitems()
    {
        return $this->hasMany('App\InvoiceItem', 'invoice_id', 'id');
    }
}
