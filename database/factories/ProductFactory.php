<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Product::class, function (Faker $faker) {


    return [
        'name' => $faker->name,
        'catogory_id' => str_random(2),
        'unit_definition' => $faker->text,
        'description' => $faker->text,
        'gst_apply' => $faker->text,
        'service_type' => $faker->text,
        'igst' => $faker->randomDigitNotNull,
        'cgst' =>$faker->randomDigitNotNull,
        'sgst' => $faker->randomDigitNotNull,
        'hsn' => $faker->randomDigitNotNull,
        'conversion_rate' => $faker->text,
        'opeaning_quantity' => $faker->randomDigitNotNull,
        'opeaning_rate' => $faker->randomDigitNotNull,
        'opeaning_value' => $faker->randomDigitNotNull,
    ];
});


