<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
| factory(App\Customer::class, 9999)->create();
*/

//$factory->define(App\User::class, function (Faker $faker) {
$factory->define(App\Customer::class, function (Faker $faker) {
   // static $password;

    return [
        /*'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),*/

        'name' => $faker->company,
        'mobile' => $faker->phoneNumber,
        'email' => $faker->email,
        'gstin' => $faker->buildingNumber,
        'person_name' => $faker->name,
        'street' => $faker->streetAddress,
        'locality' => $faker->streetName,
        'city' =>$faker->city,
        'state' => $faker->streetName,
        'country' => $faker->country,
        'pin' => $faker->buildingNumber,
    ];
});
