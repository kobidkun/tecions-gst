<?php
use Illuminate\Database\Seeder;
use App\Product;
use Faker\Generator;
class DummyDataSeeder extends Seeder{
    public function run() {
        $this->call(DummyDataSeeder::class);
    }
}