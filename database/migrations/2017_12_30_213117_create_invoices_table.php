<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->text('inv_id')->nullable();
            $table->text('inv_date_of_purchase')->nullable();
            $table->text('inv_return_month')->nullable();
            $table->text('inv_due_date')->nullable();
            $table->text('inv_customer_name')->nullable();
            $table->text('inv_customer_gstin')->nullable();
            $table->text('inv_customer_state')->nullable();
            $table->text('inv_person_name')->nullable();
            $table->text('inv_street')->nullable();
            $table->text('inv_locality')->nullable();
            $table->text('inv_city')->nullable();
            $table->text('inv_state')->nullable();
            $table->text('inv_country')->nullable();
            $table->text('inv_pin')->nullable();
            $table->text('inv_customer_shipping_address')->nullable();
            $table->text('inv_total_tax_value')->nullable();
            $table->text('inv_total_tax_cgst_value')->nullable();
            $table->text('inv_total_tax_igst_value')->nullable();
            $table->text('inv_total_tax_sgst_value')->nullable();
            $table->text('inv_total_tax_cess_value')->nullable();
            $table->text('inv_total_discount_value')->nullable();
            $table->text('inv_total_discount_percentage')->nullable();
            $table->text('inv_total_value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
