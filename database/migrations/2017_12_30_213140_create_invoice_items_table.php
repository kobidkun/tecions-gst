<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_items', function (Blueprint $table) {
            $table->increments('id');
           // $table->foreign('invoice_id')->references('id')->on('invoices');
         //   $table->integer('invoice_id')->unsigned();
            $table->text('invoice_id')->nullable();
            $table->text('item_sno')->nullable();
            $table->text('item_name')->nullable();
            $table->text('item_type')->nullable();
            $table->text('item_hsn_sac')->nullable();
            $table->text('item_qty')->nullable();
            $table->text('item_unit')->nullable();
            $table->text('item_rate')->nullable();
            $table->text('item_total_taxable_rate')->nullable();
            $table->text('item_tax_value')->nullable();
            $table->text('unit_item_tax_value')->nullable();
            $table->text('item_tax_rate_percentage')->nullable();
            $table->text('item_tax_cgst')->nullable();
            $table->text('item_tax_cgst_percentage')->nullable();
            $table->text('item_tax_sgst')->nullable();
            $table->text('item_tax_sgst_percentage')->nullable();
            $table->text('item_tax_igst')->nullable();
            $table->text('item_tax_igst_percentage')->nullable();
            $table->text('item_tax_cess')->nullable();
            $table->text('item_tax_cess_percentage')->nullable();
            $table->text('item_discount_percentage')->nullable();
            $table->text('item_discount_value')->nullable();
            $table->text('item_total')->nullable();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_items');
    }
}
