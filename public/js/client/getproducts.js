$(document).ready(function() {
    $('#example').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "api/product",
        "columns": [
            { "data": "id" },
            { "data": "name" },
            { "data": "opeaning_rate" },
            { "data": "hsn" },
            { "data": "igst" },
            { "data": "sgst" }
        ]
    } );
} );